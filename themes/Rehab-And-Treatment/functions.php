<?php
/* Dynamic Landing Page Code */

add_action( 'template_redirect', 'frn_404_intercept', 0 );
if(!function_exists('frn_404_intercept')) {
function frn_404_intercept( $type = 'page' ) {
	if(!is_admin() || !is_single()) {
	    global $wp_query, $post;
	    $char_min = 4;   //if a URL has fewer characters than this, keywords won't be extracted from URL

	    if ( $wp_query->post_count == 0 ) {  //&& $wp_query->query_vars['taxonomy'] == 'theme'


			/////////
			/// GET PHRASE FROM URL
			/////////
			global $wp;
			$domain = get_bloginfo( 'wpurl' );
			$pageURL = home_url( $wp->request ); //add_query_arg( $wp->query_string, '', home_url( $wp->request ) );
			$pageURL = str_replace($domain."/","",$pageURL);
			if(strpos($pageURL,"/")===0) $pageURL = substr($pageURL,(-1*(strlen($pageURL)))+1); //if starts with slash (for some reason), then just take everything after the slash as the pageURL. Not sure when this case occurs.
	
			$page_url_chars = strlen($pageURL);
			if($page_url_chars >=$char_min) {

				///////
				// CLEAN UP THE PHRASE
				/* NOTES:
					1. Typical landing page URL is /california-drug-rehab/?adp_var=number&var=number,etc.
					2. There should not be a first slash since domain, its slash, and http:// were removed from URL above.
					3. That leaves it starting with a word. 
					4. All PPC landing pages end with a slash and a string of variables for Dialogue Tech phone numbers.
					5. But they dont always have variables and may not always end with a slash.
					6. So we need to consider all these situations when figuring out the beginning and end of the phase.
					7. The code below will not work for child pages or custom post types that have the type in the URLs.
					8. This clears out any use of words like "McBride", where a few characters in there is another capital letter. 
						So far, there hasn't been a case. As a result, Dax opted to use a lowercase cleaner to be sure PHP formatting can be cleanly used in content.
				*/
				
				//if a slash isn't in the URL but is still not blank, store it for cleanup
				//otherwise, we don't want all the Dialogue Tech vars in our phrase...
					// So get the text to the left of the first slash found (since the one after the domain was already removed)
				$first_slash=strpos($pageURL,"/");
				if(!strpos($pageURL,"/") && $pageURL!=="") $phrase = $pageURL;  
					else $phrase = substr($pageURL,0,$first_slash); 
				$phrase = str_replace("-"," ",$phrase); // replace each dash with a space
				$phrase = str_replace("%27","'",$phrase);  //some words may need to be a contraction or look like O'Brien (as a result, they must use %27 since quotes won't work in URLs)
				$phrase = strtolower(trim($phrase)); //makes sure things are lowercase

				// set all the typical query values to fake an actual page
		        $wp_query->is_404 = false;
		        $wp_query->query['post_type'] = $type;
		        $wp_query->query['post_title'] = $phrase;
		        $post = new stdClass();
		        $post->post_type = $wp_query->query['post_type'];
				$post->post_title = $phrase;

		        status_header( '200' );
		        //var_dump(http_response_code ()); exit;
		        $custom_template = 'landing-pages-template.php';

		        //echo "template: ".$custom_template;
		        // Load template?
				//if ( $template ) {
					$template_found = locate_template( $custom_template, true );
					if ( $template_found ) exit; //echo "<br />template found!";
					//else echo "<br />template NOT found";
					//exit;
				//}
			}
	    }
	}
}
}

if(!function_exists('titleCase')) {
function titleCase ($title) {
	//remove HTML, storing it for later
	//       HTML elements to ignore    | tags  | entities
	$regx = '/<(code|var)[^>]*>.*?<\/\1>|<[^>]+>|&\S+;/';
	preg_match_all ($regx, $title, $html, PREG_OFFSET_CAPTURE);
	$title = preg_replace ($regx, '', $title);
	
	//find each word (including punctuation attached)
	preg_match_all ('/[\w\p{L}&`\'‘’"“\.@:\/\{\(\[<>_]+-? */u', $title, $m1, PREG_OFFSET_CAPTURE);
	foreach ($m1[0] as &$m2) {
		//shorthand these- "match" and "index"
		list ($m, $i) = $m2;
		
		//correct offsets for multi-byte characters (`PREG_OFFSET_CAPTURE` returns *byte*-offset)
		//we fix this by recounting the text before the offset using multi-byte aware `strlen`
		$i = mb_strlen (substr ($title, 0, $i), 'UTF-8');
		
		//find words that should always be lowercase…
		//(never on the first word, and never if preceded by a colon)
		$m = $i>0 && mb_substr ($title, max (0, $i-2), 1, 'UTF-8') !== ':' && 
			!preg_match ('/[\x{2014}\x{2013}] ?/u', mb_substr ($title, max (0, $i-2), 2, 'UTF-8')) && 
			 preg_match ('/^(a(nd?|s|t)?|b(ut|y)|en|for|i[fn]|o[fnr]|t(he|o)|vs?\.?|via)[ \-]/i', $m)
		?	//…and convert them to lowercase
			mb_strtolower ($m, 'UTF-8')
			
		//else:	brackets and other wrappers
		: (	preg_match ('/[\'"_{(\[‘“]/u', mb_substr ($title, max (0, $i-1), 3, 'UTF-8'))
		?	//convert first letter within wrapper to uppercase
			mb_substr ($m, 0, 1, 'UTF-8').
			mb_strtoupper (mb_substr ($m, 1, 1, 'UTF-8'), 'UTF-8').
			mb_substr ($m, 2, mb_strlen ($m, 'UTF-8')-2, 'UTF-8')
			
		//else:	do not uppercase these cases
		: (	preg_match ('/[\])}]/', mb_substr ($title, max (0, $i-1), 3, 'UTF-8')) ||
			preg_match ('/[A-Z]+|&|\w+[._]\w+/u', mb_substr ($m, 1, mb_strlen ($m, 'UTF-8')-1, 'UTF-8'))
		?	$m
			//if all else fails, then no more fringe-cases; uppercase the word
		:	mb_strtoupper (mb_substr ($m, 0, 1, 'UTF-8'), 'UTF-8').
			mb_substr ($m, 1, mb_strlen ($m, 'UTF-8'), 'UTF-8')
		));
		
		//resplice the title with the change (`substr_replace` is not multi-byte aware)
		$title = mb_substr ($title, 0, $i, 'UTF-8').$m.
			 mb_substr ($title, $i+mb_strlen ($m, 'UTF-8'), mb_strlen ($title, 'UTF-8'), 'UTF-8')
		;
	}
	
	//restore the HTML
	foreach ($html[0] as &$tag) $title = substr_replace ($title, $tag[0], $tag[1], 0);
	echo $title;
}
}



// Theme support options
require_once(get_template_directory().'/assets/functions/theme-support.php'); 

// WP Head and other cleanup functions
require_once(get_template_directory().'/assets/functions/cleanup.php'); 

// Register scripts and stylesheets
require_once(get_template_directory().'/assets/functions/enqueue-scripts.php'); 

// Register custom menus and menu walkers
require_once(get_template_directory().'/assets/functions/menu.php'); 

// Register sidebars/widget areas
require_once(get_template_directory().'/assets/functions/sidebar.php'); 

// Makes WordPress comments suck less
require_once(get_template_directory().'/assets/functions/comments.php'); 

// Replace 'older/newer' post links with numbered navigation
require_once(get_template_directory().'/assets/functions/page-navi.php'); 

// Adds support for multiple languages
require_once(get_template_directory().'/assets/translation/translation.php'); 


function my_acf_init() {
	
	acf_update_setting('google_api_key', 'AIzaSyCuGEkS3XOItflpYiLMK5ClgulNVI1l9TM');
}

add_action('acf/init', 'my_acf_init');


// Remove 4.2 Emoji Support
// require_once(get_template_directory().'/assets/functions/disable-emoji.php'); 

// Adds site styles to the WordPress editor
//require_once(get_template_directory().'/assets/functions/editor-styles.php'); 

// Related post function - no need to rely on plugins
// require_once(get_template_directory().'/assets/functions/related-posts.php'); 

// Use this as a template for custom post types
// require_once(get_template_directory().'/assets/functions/custom-post-type.php');

// Customize the WordPress login menu
// require_once(get_template_directory().'/assets/functions/login.php'); 

// Customize the WordPress admin
// require_once(get_template_directory().'/assets/functions/admin.php'); 