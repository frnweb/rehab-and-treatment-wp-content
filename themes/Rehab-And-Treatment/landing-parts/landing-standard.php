	<section class="gold-standard" id="our-success">
			<div class="pure-g">
				<div class="pure-u-1">
					<h3 class="is-center">Foundations Recovery Network's</h3>
					<h2 class="is-center">Gold Standard</h2>
					<img src="/wp-content/themes/Rehab-And-Treatment/mobile-images/frn-gold-standard.svg" alt="Foundations Recovery Network's Gold Standard"><p class="paddingcentered">Through our integrated treatment approach, our programs annually meet criteria for “Dual Diagnosis Enhanced” (DDE) services, a standard only achieved by the top 5% of addiction treatment programs. Eligibility for DDE services is determined by the Dual Diagnosis Capability in Addiction Treatment (DDCAT) index assessment. In 2007, Dr. Mark McGovern, a member of Dartmouth Psychiatric Research Center and one of the developers of the DDCAT, classified FRN’s program as the “gold standard” in the treatment industry.</p>
				</div>
			</div>
		</section>