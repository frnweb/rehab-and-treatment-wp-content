		<section class="integrated-treatment" id="integrated-treatment">
			<a name="modal2"></a><a name="modal"></a>

			<div class="is-center">
				<h2>How Does Integrated Treatment Work?</h2>
				<p>Integrated Treatment focuses on addressing the whole person, not just a single condition. That’s why we treat mental health issues and addictive behavior together, at the same time. Below you’ll find some of the most common substances and mental health diagnoses we treat.</p>
			</div>
			
			
			
			<div class="card">
			
				<h2>Mental Health</h2>
				<div class="card--inner">
					<ul>
						<li>Depression</li>
						<li>Anxiety</li>
						<li>Mood Disorders</li>

					</ul><!-- end ul -->
				
					<ul>
						<li>Bipolar Disorder</li>
						<li>PTSD </li>
						<li>Borderline Personality Disorder </li>
					</ul><!-- end ul -->
				</div><!--- end card--innner -->
			</div>
			<!-- /.card -->	
			
			<div class="plus-symbol"></div>
			<!-- /.plus-symbol -->
			
				<div class="card right">
				
				<h2>Substance Abuse</h2>
				<div class="card--inner">
					<ul>
						<li>Heroin</li>
						<li>Prescription Pills</li> 
						<li>Hydrocodone</li>
						<li>Opiates/Opioids</li> 
						<li>Alcohol</li>
					</ul><!-- end ul -->
					
					<ul>
						<li>Xanax</li>
						<li>Benzos</li> 
						<li>Suboxone</li>
						<li>Klonopin</li> 
						<li>Subutex</li> 
					</ul><!-- end ul -->
					</div><!--- end card--innner -->
				
			</div>
			<!-- /.card -->		
			

		</section><!-- ends integrated treatment -->