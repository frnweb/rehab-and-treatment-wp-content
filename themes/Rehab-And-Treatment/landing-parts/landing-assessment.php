<section class="cta-phone" id="risk-assessment">
			<div class="pure-g">
				<div class="pure-u-1 pure-u-md-1-1 pure-u-lg-1-3">
					<h3 class="">Take an Addiction Risk Assessment</h3><p>This brief, confidential online quiz will help determine if you or a loved one has an addiction. It’s easy to fill out and takes less than four minutes!</p>
				</div>
				<div class="pure-u-1 pure-u-md-1-1 pure-u-lg-2-3">
					<div class="pure-g">
					<div class="pure-u-1-1 pure-u-md-1-2 first-assmt">
						<div class="assessments">
						<h2 class="is-center">Take an assessment for a family member, loved one or friend</h3>
						<a class="typeform-share button"  data-ga-event="Loved One Assessment" data-ga-category="Assessment" data-ga-action="Opened Loved One Assessment" data-ga-label="Loved One Assessment" href="https://foundationsrecoverynetwork.typeform.com/to/b5zAE0" data-mode="popup" style="display:inline-block;text-decoration:none;background-color:#5A9F7B;color:white;cursor:pointer;font-family:Helvetica,Arial,sans-serif;font-size:17px;line-height:42.5px;text-align:center;margin:0;height:42.5px;padding:0px 28px;border-radius:0px;max-width:100%;white-space:nowrap;overflow:hidden;text-overflow:ellipsis;font-weight:bold;-webkit-font-smoothing:antialiased;-moz-osx-font-smoothing:grayscale;" target="_blank">Loved one assessment </a> <script> (function() { var qs,js,q,s,d=document, gi=d.getElementById, ce=d.createElement, gt=d.getElementsByTagName, id="typef_orm_share", b="https://embed.typeform.com/"; if(!gi.call(d,id)){ js=ce.call(d,"script"); js.id=id; js.src=b+"embed.js"; q=gt.call(d,"script")[0]; q.parentNode.insertBefore(js,q) } })() </script>
						</div>
					</div>
					<div class="pure-u-1-1 pure-u-md-1-2 second-assmt">
						<div class="assessments last">
						<h2 class="is-center">Take an assessment for myself</h3>
						<a class="typeform-share button" data-ga-event="Self Assessment" data-ga-category="Assessment" data-ga-action="Opened Self Assessment" data-ga-label="Self Assessment" href="https://foundationsrecoverynetwork.typeform.com/to/Gmq3aX" data-mode="popup" style="display:inline-block;text-decoration:none;background-color:#5A9F7B;color:white;cursor:pointer;font-family:Helvetica,Arial,sans-serif;font-size:17px;line-height:42.5px;text-align:center;margin:0;height:42.5px;padding:0px 28px;border-radius:0px;max-width:100%;white-space:nowrap;overflow:hidden;text-overflow:ellipsis;font-weight:bold;-webkit-font-smoothing:antialiased;-moz-osx-font-smoothing:grayscale;" target="_blank">Self-assessment </a> <script> (function() { var qs,js,q,s,d=document, gi=d.getElementById, ce=d.createElement, gt=d.getElementsByTagName, id="typef_orm_share", b="https://embed.typeform.com/"; if(!gi.call(d,id)){ js=ce.call(d,"script"); js.id=id; js.src=b+"embed.js"; q=gt.call(d,"script")[0]; q.parentNode.insertBefore(js,q) } })() </script>
						</div>
						</div>
				</div>
			</div>
			</div>
		</section>