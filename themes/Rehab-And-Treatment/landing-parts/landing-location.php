<section class="our-locations" id="our-locations">
		<div class="inner">
			<div class="pure-g">
				<div class="pure-u-1 pure-u-md-1-1 pure-u-lg-1-2">
					<h2>Nationwide Locations</h2>
					<p>We are one of the largest networks of treatment centers in the country, so our clients from <?php if($state=="") echo "[none]";else echo $state; ?>  have a wide selection of locations from which to choose. Some prefer to find a place close to home, while others choose to leave their current environments completely so they can begin healing in a fresh setting.</p>
					<p>The most important part of choosing the right place for treatment is finding a place customized around your unique needs, which is our specialty. And if for some reason you feel our locations aren’t a good fit, we can help you get connected to other treatment centers because of our trusted referral relationships. Ultimately, you or loved one will be taken care of in a place of safety and healing.</p>
					<!--<div class="locationstabs" style="display:none;">
						<h6><span>Popular Cities we serve in <?php if($state=="") echo "[none]";else echo $state; ?>  <i class="fa fa-plus fa-lg"></i></span></h6>
						<h6><span>Counties we serve in <?php if($state=="") echo "[none]";else echo $state; ?>  <i class="fa fa-plus fa-lg"></i></span></h6>
					</div>-->
				</div>
				
				<div class="pure-u-1 pure-u-md-1-1 pure-u-lg-1-2">
				<h3 class="is-center">Let us help find the best location that meets your individual needs</h3>
				<div class="divider"></div>
					<span class="location--phone is-center"><?php echo do_shortcode('[frn_phone number="(877) 345-3221" action="Phone Clicks in Location Section"]'); ?></span>
				</div><!-- end column -->
			</div>
	</div><!-- end inner -->
		</section>