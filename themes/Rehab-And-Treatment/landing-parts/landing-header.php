<div class="header">
		<nav class="home-menu pure-menu pure-menu-horizontal pure-menu-fixed" id="frn-nav">
		    <div class="inner">
		    	<a class="pure-menu-heading" href=""><img src="/wp-content/themes/Rehab-And-Treatment/mobile-images/foundations-1.svg">
		     	</a>
		   		 <ul class="pure-menu-list mobile-hide">
		        	<!-- <li class="pure-menu-item"><a href="#our-success" class="pure-menu-link smoothscroll">Our Success</a></li> -->
		        	<li class="pure-menu-item"><a href="#our-locations" class="pure-menu-link smoothscroll">Locations</a></li>
		        	<li class="pure-menu-item"><a href="#cost-of-treatment" class="pure-menu-link smoothscroll">Cost of Treatment</a></li>
		        	<li class="pure-menu-item"><a href="#program-details" class="pure-menu-link smoothscroll">Programs</a></li>
		        	<li class="pure-menu-item"><a href="#select-your-plan" class="pure-menu-link smoothscroll">Select Your Plan</a></li>
		        	
					 <li class="pure-menu-item"><span class="header--phone"><?php echo do_shortcode('[frn_phone number="(877) 345-3221" action="Phone Clicks in Landing Page Top Header"]'); ?></span></li><!-- this is the new phone number -->
		   		 </ul>
		    </div>
	    </nav>
	    <!-- floating footer bar -->
	    <div id="mobile-nav">
	    	<div class="pure-g">
				<div class="pure-u-1-5 pure-u-md-1-3"><?php 
					echo do_shortcode('[lhn_inpage button="email" text="empty" desktop="Email" class="fa fa-envelope" category="Contact Options Bar" action="Email Clicks" ]'); 
					/* 
					// Dax commented this out via PHP instead of HTML to keep HTML cleaner 10/18/17
					?><a onclick="window.open('http://www.livehelpnow.net/lhn/TicketsVisitor.aspx?lhnid=14160','Ticket','left=' + (screen.width - 550-32) / 2 + ',top=50,scrollbars=yes,menubar=no,height=550,width=450,resizable=yes,toolbar=no,location=no,status=no');return false;"><i class="fa fa-envelope" aria-hidden="true"></i><span class="mobile-hide">Email</span></a><?php */ 
				?></div>

				<div class="pure-u-3-5 number pure-u-md-1-3"><i class="fa fa-mobile mobile-hide" aria-hidden="true"></i><span class="phone-number-fixed"><?php 
					echo do_shortcode('[frn_phone number="(877) 345-3221" action="Phone Clicks in Landing Page Menu Bar"]'); 
				?></span></div>

				<div class="pure-u-1-5 pure-u-md-1-3"><?php 
					echo do_shortcode('[lhn_inpage button="chat" text="empty" desktop="Live Chat" offline_mobile="empty" class="fa fa-comments-o" category="Contact Options Bar" action="Chat Clicks" ]'); 
						/* 
						// Dax commented this out via PHP instead of HTML to keep HTML cleaner 10/18/17
						?><a onclick="OpenLHNChat();return false; ga('send', 'event', 'Contact Options Flyout', 'Chat/Email');"><i class="fa fa-comments-o" aria-hidden="true"></i><span class="mobile-hide">Live Chat</span><?php */ 
				?></div>
			</div>
		</div><!-- end floating footer bar -->
	</div><!-- end header -->