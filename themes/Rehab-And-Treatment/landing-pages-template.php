<?php if ( ! defined( 'ABSPATH' ) ) { exit; }  // File Security Check 

	////////
	/// LOGIC SETTINGS
	////////

	$debug_users = array(
			"daxon.edwards",
			"ben.wright",
			"jonny.stoval",
			"cherie.carter",
			"matt.king",
			"sean,obriant"
		);



	////////
	/// DEBUG NOTIFICATIONS SWITCH BY USER
	////////

	$debug=false; $debug_notice="";

	/*
	The following is not working. Disabling for clarity.
	global $current_user;
	get_currentuserinfo();
	$current_usernm = $current_user->user_login;
	echo $current_usernm;
	//if the page visitor is logged in and if it's an dev admin username, activate debug notices in the HEAD content of the page
	//loop through list of debug users above
	if($current_usernm!=="") {
		foreach($debug_users as $frn_user) {
			if($current_usernm==$frn_user) $debug=true;
		}
	}
	if($debug) $debug_notice = "
	
		<!--
		////////////////////
		////////////////////
		
		CURRENT USER: ".$current_usernm."
		
		////////////////////
		////////////////////
		-->
	
		";
	
	*/




	////////
	/// GET WORDS FROM TITLE / ASSIGN VARS FOR CONTENT
	////////
		
	$phrase=get_the_title();
		
	///////
	// FIND WORDS DEFINED IN PPC MAIN SETTINGS IN URL (OFTEN CITIES)
	$frn_ppc_settings = get_option('frn_ppc_settings');
	$manual_words=""; $state = "";
	if(isset($frn_ppc_settings['list'])) {
		if(trim($frn_ppc_settings['list'])!=="") {
			//search using manual list
			$improve_commas = str_replace(", ",",",$frn_ppc_settings['list']); //makes sure there aren't spaces after the comma
			$manual_list = explode(",",$improve_commas); //creates array
			$total_list = count($manual_list); $c=0;
			while ($manual_words==="" && $c<>$total_list) {
				if($manual_list[$c]!=="") {
					if(strpos($phrase,strtolower($manual_list[$c]))!== false) {
						$manual_words = trim($manual_list[$c]);
						if($debug) $debug_notice .= "
	
	<!--
	////////////////////
	////////////////////
			
		Manual Words Found: ".$manual_list[$c]."
		
	////////////////////
	////////////////////
		
	-->

					";
					} // end if manual list array item found
				} // end requirement individual manual list array isn't blank
				$c++;
			}  // end while loop
		}  // end requirement that something is even entered in the PPC settings for manually entered cities or items
	}  // ends the requirement that the variable is even defined
	

	// STATES ARRAY WITH BACKGROUND IMAGES AND TOP 10 CITIES
	$states = array (
		array("Charleston SC","/wp-content/uploads/shutterstock_1932706-min.jpg",),
		array("Columbus Georgia","/wp-content/uploads/shutterstock_188444174-min.jpg",),
		array("Jacksonville","/wp-content/uploads/shutterstock_29100910.jpg",),
		array("Portland Maine","/wp-content/uploads/shutterstock_217445746.jpg",),
		array("Charleston","/wp-content/uploads/shutterstock_55288861.jpg",),
		array("Columbus","/wp-content/uploads/shutterstock_157876028.jpg",),
		array("Jackson","/wp-content/uploads/shutterstock_183253658-min.jpg",),
		array("Portland","/wp-content/uploads/shutterstock_275640608.jpg",),
		array("12-Step","/wp-content/uploads/shutterstock_361011203.jpg",),
		array("Albuquerque","/wp-content/uploads/shutterstock_8014669-1.jpg",),
		array("Anchorage","/wp-content/uploads/shutterstock_798028152.jpg",),
		array("Atlanta","/wp-content/uploads/shutterstock_2093628791.jpg",),
		array("Aurora","/wp-content/uploads/shutterstock_203723848.jpg",),
		array("Baltimore","/wp-content/uploads/shutterstock_278551604.jpg",),
		array("Baton Rouge","/wp-content/uploads/shutterstock_1932706-min.jpg",),
		array("Billings","/wp-content/uploads/shutterstock_135615797.jpg",),
		array("Birmingham","/wp-content/uploads/shutterstock_101142673-1.jpg",),
		array("Bismarck","/wp-content/uploads/shutterstock_153302879-min.jpg",),
		array("Boise","/wp-content/uploads/shutterstock_153209951.jpg",),
		array("Boston","/wp-content/uploads/shutterstock_2721717471.jpg",),
		array("Bridgeport","/wp-content/uploads/shutterstock_12598006.jpg",),
		array("Burlington","/wp-content/uploads/shutterstock_194728145-min.jpg",),
		array("Casper","/wp-content/uploads/shutterstock_46815583.jpg",),
		array("Cedar Rapids","/wp-content/uploads/shutterstock_46815583.jpg",),
		array("Charlotte","/wp-content/uploads/shutterstock_225114895.jpg",),
		array("Cheyenne","/wp-content/uploads/shutterstock_110304029-min.jpg",),
		array("Chicago","/wp-content/uploads/shutterstock_203723848.jpg",),
		array("Christian","/wp-content/uploads/shutterstock_361011203.jpg",),
		array("Cleveland","/wp-content/uploads/shutterstock_246045253-min.jpg",),
		array("Colorado Springs","/wp-content/uploads/shutterstock_13144351-min.jpg",),
		array("Columbia","/wp-content/uploads/shutterstock_137052038.jpg",),
		array("Denver","/wp-content/uploads/shutterstock_158034125-min.jpg",),
		array("Des Moines","/wp-content/uploads/shutterstock_34487929.jpg",),
		array("Detroit","/wp-content/uploads/shutterstock_353680631.jpg",),
		array("Dover","/wp-content/uploads/shutterstock_125454413.jpg",),
		array("Essex","/wp-content/uploads/shutterstock_125454413.jpg",),
		array("Faith-Based","/wp-content/uploads/shutterstock_361011203.jpg",),
		array("Fargo","/wp-content/uploads/shutterstock_153302879-min.jpg",),
		array("Fort Smith","/wp-content/uploads/shutterstock_243048247-min.jpg",),
		array("Fort Wayne","/wp-content/uploads/2015/03/sunset.jpg",),
		array("Gay","/wp-content/uploads/shutterstock_321695663-min.jpg",),
		array("Grand Rapids","/wp-content/uploads/shutterstock_186886109-min.jpg",),
		array("Gulfport","/wp-content/uploads/shutterstock_1932706-min.jpg",),
		array("Hempstead","/wp-content/uploads/shutterstock_125454413.jpg",),
		array("Henderson","/wp-content/uploads/shutterstock_254984635.jpg",),
		array("Honolulu","/wp-content/uploads/shutterstock_143622016-min.jpg",),
		array("Houston","/wp-content/uploads/shutterstock_149344304-min.jpg",),
		array("Huntington","/wp-content/uploads/shutterstock_46815583.jpg",),
		array("Indianapolis","/wp-content/uploads/shutterstock_2708329971.jpg",),
		array("Jersey City","/wp-content/uploads/shutterstock_71940337-min.jpg",),
		array("Juneau","/wp-content/uploads/shutterstock_68685307-min.jpg",),
		array("Kansas City","/wp-content/uploads/shutterstock_102618359.jpg",),
		array("Las Cruces","/wp-content/uploads/shutterstock_254984635.jpg",),
		array("Las Vegas","/wp-content/uploads/shutterstock_62627692.jpg",),
		array("Lesbian","/wp-content/uploads/shutterstock_127194344-min.jpg",),
		array("Lewiston","/wp-content/uploads/shutterstock_125454413.jpg",),
		array("Lexington","/wp-content/uploads/shutterstock_292723040-1.jpg",),
		array("LGBT","/wp-content/uploads/shutterstock_379525111-min.jpg",),
		array("Lincoln","/wp-content/uploads/shutterstock_46815583.jpg",),
		array("Little Rock","/wp-content/uploads/shutterstock_140798233.jpg",),
		array("Los Angeles","/wp-content/uploads/shutterstock_251304709.jpg",),
		array("Louisville","/wp-content/uploads/shutterstock_38375269-min.jpg",),
		array("Madison","/wp-content/uploads/shutterstock_1762282-1.jpg",),
		array("Manchester","/wp-content/uploads/shutterstock_125454413.jpg",),
		array("Memphis","/wp-content/uploads/shutterstock_268856306-min.jpg",),
		array("Miami","/wp-content/uploads/shutterstock_322498040-min.jpg",),
		array("Mililani","/wp-content/uploads/shutterstock_143622016-min.jpg",),
		array("Milwaukee","/wp-content/uploads/shutterstock_247301527.jpg",),
		array("Minneapolis","/wp-content/uploads/shutterstock_286585523.jpg",),
		array("Missoula","/wp-content/uploads/shutterstock_284793851-min.jpg",),
		array("Montgomery","/wp-content/uploads/2015/03/sunset.jpg",),
		array("Nampa","/wp-content/uploads/shutterstock_1728816172.jpg",),
		array("Nashua","/wp-content/uploads/shutterstock_125454413.jpg",),
		array("Nashville","/wp-content/uploads/shutterstock_268855592-min.jpg",),
		array("New Haven","/wp-content/uploads/shutterstock_12598006.jpg",),
		array("New Orleans","/wp-content/uploads/shutterstock_313879175.jpg",),
		array("Newark","/wp-content/uploads/shutterstock_152247299.jpg",),
		array("Non-12 Step","/wp-content/uploads/shutterstock_361011203.jpg",),
		array("Norfolk","/wp-content/uploads/shutterstock_273393128-min.jpg",),
		array("NYC","/wp-content/uploads/shutterstock_325338227.jpg",),
		array("Oklahoma City","/wp-content/uploads/shutterstock_188211803.jpg",),
		array("Omaha","/wp-content/uploads/shutterstock_27183652.jpg",),
		array("Overland Park","/wp-content/uploads/shutterstock_102618359.jpg",),
		array("Phoenix","/wp-content/uploads/shutterstock_119178070-min.jpg",),
		array("Pittsburgh","/wp-content/uploads/shutterstock_284869697-min.jpg",),
		array("Providence","/wp-content/uploads/shutterstock_231462994-1.jpg",),
		array("Raleigh","/wp-content/uploads/shutterstock_271073756-min.jpg",),
		array("Rapid City","/wp-content/uploads/shutterstock_83190301-min.jpg",),
		array("Rehab For Men","/wp-content/uploads/shutterstock_2167883441.jpg",),
		array("Rehab For Women","/wp-content/uploads/shutterstock_2659013691.jpg",),
		array("Rehab For Young Adults","/wp-content/uploads/shutterstock_1824836511.jpg",),
		array("Saint Paul","/wp-content/uploads/shutterstock_1728816172.jpg",),
		array("Salem","/wp-content/uploads/shutterstock_2438071661.jpg",),
		array("Salt Lake City","/wp-content/uploads/shutterstock_203101042.jpg",),
		array("San Antonio","/wp-content/uploads/shutterstock_120141397-min.jpg",),
		array("San Diego","/wp-content/uploads/shutterstock_248702272-min.jpg",),
		array("Seattle","/wp-content/uploads/shutterstock_33117430.jpg",),
		array("Silver Spring","/wp-content/uploads/shutterstock_125454413.jpg",),
		array("Sioux Falls","/wp-content/uploads/shutterstock_111118415-1.jpg",),
		array("Spokane","/wp-content/uploads/shutterstock_284794043-min.jpg",),
		array("St. Louis","/wp-content/uploads/shutterstock_288427781-min.jpg",),
		array("Tucson","/wp-content/uploads/shutterstock_17943163-min.jpg",),
		array("Tulsa","/wp-content/uploads/shutterstock_46815583.jpg",),
		array("Virginia Beach","/wp-content/uploads/shutterstock_167465321.jpg",),
		array("Warwick","/wp-content/uploads/shutterstock_125454413.jpg",),
		array("West Valley City","/wp-content/uploads/shutterstock_254984635.jpg",),
		array("Wichita","/wp-content/uploads/shutterstock_175221431-min.jpg",),
		array("Wilmington","/wp-content/uploads/shutterstock_218419345.jpg",),
		array("Worcester","/wp-content/uploads/shutterstock_125454413.jpg",),
		array("Washington DC","/wp-content/uploads/shutterstock_125454413.jpg",),
		array("Alabama","/wp-content/uploads/2015/03/sunset.jpg",array("Auburn","Birmingham","Decatur","Dothan","Hoover","Huntsville","Madison","Mobile","Montgomery","Tuscaloosa")),
		array("Alaska","/wp-content/uploads/shutterstock_1728816172.jpg",array("Anchorage","Bethel","Fairbanks","Juneau","Kenai","Ketchikan","Kodiak","Palmer","Sitka","Wasilla")),
		array("Arizona","/wp-content/uploads/shutterstock_254984635.jpg",array("Chandler","Gilbert","Glendale","Mesa","Peoria","Phoenix","Scottsdale","Surprise","Tempe","Tucson")),
		array("Arkansas","/wp-content/uploads/2015/03/sunset.jpg",array("Bentonville","Conway","Fayetteville","Fort Smith","Jonesboro","Little Rock","North Little Rock","Pine Bluff","Rogers","Springdale")),
		array("California","/wp-content/uploads/shutterstock_259776827.jpg",array("Anaheim","Bakersfield","Fresno","Long Beach","Los Angeles","Oakland","Sacramento","San Diego","San Francisco","San Jose")),
		array("Colorado","/wp-content/uploads/shutterstock_233274136-min.jpg",array("Arvada","Aurora","Centennial","Colorado Springs","Denver","Fort Collins","Lakewood","Pueblo","Thornton","Westminster")),
		array("Connecticut","/wp-content/uploads/shutterstock_125454413.jpg",array("Bridgeport","Bristol","Danbury","Hartford","Meriden","New Britain","New Haven","Norwalk","Stamford","Waterbury")),
		array("Delaware","/wp-content/uploads/shutterstock_125454413.jpg",array("Dover","Elsmere","Georgetown","Middletown","Milford","New Castle","Newark","Seaford","Smyrna","Wilmington")),
		array("Florida","/wp-content/uploads/shutterstock_173281454-min.jpg",array("Cape Coral","Fort Lauderdale","Hialeah","Jacksonville","Miami","Orlando","Port St. Lucie","St. Petersburg","Tallahassee","Tampa")),
		array("Georgia","/wp-content/uploads/shutterstock_1932706-min.jpg",array("Albany","Athens","Atlanta","Augusta","Columbus","Johns Creek","Macon","Roswell","Sandy Springs","Savannah")),
		array("Hawaii","/wp-content/uploads/shutterstock_1792119501.jpg",array("‘Ewa Gentry ","Hilo","Honolulu","Kahului ","Kailua","Kaneohe ","Mililani Town ","Pearl City","Urban Honolulu CDP","Waipahu ")),
		array("Idaho","/wp-content/uploads/shutterstock_1728816172.jpg",array("Boise City","Caldwell","Coeur d'Alene","Idaho Falls","Lewiston","Meridian","Nampa","Pocatello","Post Falls","Twin Falls")),
		array("Illinois","/wp-content/uploads/shutterstock_46815583.jpg",array("Aurora","Champaign","Chicago","Elgin","Joliet","Naperville","Peoria","Rockford","Springfield","Waukegan")),
		array("Indiana","/wp-content/uploads/2015/03/sunset.jpg",array("Bloomington","Carmel","Evansville","Fishers","Fort Wayne","Gary","Hammond","Indianapolis","Lafayette","South Bend")),
		array("Iowa","/wp-content/uploads/shutterstock_46815583.jpg",array("Ames","Cedar Rapids","Council Bluffs","Davenport","Des Moines","Dubuque","Iowa City","Sioux City","Waterloo","West Des Moines")),
		array("Kansas","/wp-content/uploads/shutterstock_46815583.jpg",array("Kansas City","Lawrence","Lenexa","Manhattan","Olathe","Overland Park","Salina","Shawnee","Topeka","Wichita")),
		array("Kentucky","/wp-content/uploads/shutterstock_46815583.jpg",array("Bowling Green","Covington","Florence","Georgetown","Hopkinsville","Lexington","Louisville","Nicholasville","Owensboro","Richmond")),
		array("Louisiana","/wp-content/uploads/shutterstock_1932706-min.jpg",array("Alexandria","Baton Rouge","Bossier City","Houma","Kenner","Lafayette","Lake Charles","Monroe","New Orleans","Shreveport")),
		array("Maine","/wp-content/uploads/shutterstock_125454413.jpg",array("Auburn","Augusta","Bangor","Biddeford","Lewiston","Portland","Saco","Sanford","South Portland","Westbrook")),
		array("Maryland","/wp-content/uploads/shutterstock_125454413.jpg",array("Annapolis","Baltimore","Bowie","College Park","Frederick","Gaithersburg","Hagerstown","Laurel","Rockville","Salisbury")),
		array("Massachusetts","/wp-content/uploads/shutterstock_125454413.jpg",array("Boston","Brockton","Cambridge","Lowell","Lynn","New Bedford","Newton","Quincy","Springfield","Worcester")),
		array("Michigan","/wp-content/uploads/shutterstock_125454413.jpg",array("Ann Arbor","Dearborn","Detroit","Flint","Grand Rapids","Lansing","Livonia","Sterling Heights","Troy","Warren")),
		array("Minnesota","/wp-content/uploads/shutterstock_1728816172.jpg",array("Bloomington","Brooklyn Park","Duluth","Maple Grove","Minneapolis","Plymouth","Rochester","St. Cloud","St. Paul","Woodbury")),
		array("Mississippi","/wp-content/uploads/shutterstock_1932706-min.jpg",array("Biloxi","Greenville","Gulfport","Hattiesburg","Horn Lake","Jackson","Meridian","Olive Branch","Southaven","Tupelo")),
		array("Missouri","/wp-content/uploads/shutterstock_125454413.jpg",array("Columbia","Independence","Kansas City","Lee's Summit","O'Fallon","Springfield","St. Charles","St. Joseph","St. Louis","St. Peters")),
		array("Montana","/wp-content/uploads/shutterstock_233274136-min.jpg",array("Anaconda-Deer Lodge County","Billings","Bozeman","Butte-Silver Bow","Great Falls","Havre","Helena","Kalispell","Miles City","Missoula")),
		array("Nebraska","/wp-content/uploads/shutterstock_46815583.jpg",array("Bellevue","Columbus","Fremont","Grand Island","Hastings","Kearney","Lincoln","Norfolk","North Platte","Omaha")),
		array("Nevada","/wp-content/uploads/shutterstock_254984635.jpg",array("Boulder City","Carson City","Elko","Fernley","Henderson","Las Vegas","Mesquite","North Las Vegas","Reno","Sparks")),
		array("New Hampshire","/wp-content/uploads/shutterstock_125454413.jpg",array("Claremont","Concord","Dover","Keene","Laconia","Lebanon","Manchester","Nashua","Portsmouth","Rochester")),
		array("New Jersey","/wp-content/uploads/shutterstock_125454413.jpg",array("Bayonne","Camden","Clifton","Elizabeth","Jersey City","Newark","Passaic","Paterson","Trenton","Union City")),
		array("New Mexico","/wp-content/uploads/shutterstock_254984635.jpg",array("Alamogordo","Albuquerque","Carlsbad","Clovis","Farmington","Hobbs","Las Cruces","Rio Rancho","Roswell","Santa Fe")),
		array("New York","/wp-content/uploads/shutterstock_125454413.jpg",array("Albany","Buffalo","Mount Vernon","New Rochelle","New York","Rochester","Schenectady","Syracuse","Utica","Yonkers")),
		array("North Carolina","/wp-content/uploads/shutterstock_173281454-min.jpg",array("Cary","Charlotte","Durham","Fayetteville","Greensboro","Greenville","High Point","Raleigh","Wilmington","Winston-Salem")),
		array("North Dakota","/wp-content/uploads/shutterstock_46815583.jpg",array("Bismarck","Dickinson","Fargo","Grand Forks","Jamestown","Mandan","Minot","Wahpeton","West Fargo","Williston")),
		array("Ohio","/wp-content/uploads/shutterstock_125454413.jpg",array("Akron","Canton","Cincinnati","Cleveland","Columbus","Dayton","Lorain","Parma","Toledo","Youngstown")),
		array("Oklahoma","/wp-content/uploads/shutterstock_46815583.jpg",array("Broken Arrow","Edmond","Enid","Lawton","Midwest City","Moore","Norman","Oklahoma City","Stillwater","Tulsa")),
		array("Oregon","/wp-content/uploads/shutterstock_233274136-min.jpg",array("Beaverton","Bend","Corvallis","Eugene","Gresham","Hillsboro","Medford","Portland","Salem","Springfield")),
		array("Pennsylvania","/wp-content/uploads/shutterstock_125454413.jpg",array("Allentown","Altoona","Bethlehem","Erie","Harrisburg","Lancaster","Philadelphia","Pittsburgh","Reading","Scranton")),
		array("Philadelphia","/wp-content/uploads/shutterstock_108957686.jpg",array("Pittsburgh","Reading","Scranton","Rhode Island","Central Falls","Cranston","East Providence","Newport","Pawtucket","Providence")),
		array("Rhode Island","/wp-content/uploads/shutterstock_125454413.jpg",array("Central Falls","Cranston","East Providence","Newport","Pawtucket","Providence","Warwick","Woonsocket","South Carolina","Charleston")),
		array("South Carolina","/wp-content/uploads/shutterstock_173281454-min.jpg",array("Charleston","Columbia","Goose Creek","Greenville","Hilton Head Island","Mount Pleasant","North Charleston","Rock Hill","Summerville","Sumter")),
		array("South Dakota","/wp-content/uploads/shutterstock_1728816172.jpg",array("Aberdeen","Brookings","Huron","Mitchell","Pierre","Rapid City","Sioux Falls","Spearfish","Watertown","Yankton")),
		array("Tennessee","/wp-content/uploads/2015/03/sunset.jpg",array("Bartlett","Chattanooga","Clarksville","Franklin","Jackson","Johnson City","Knoxville","Memphis","Murfreesboro","Nashville")),
		array("Texas","/wp-content/uploads/shutterstock_125454413.jpg",array("Arlington","Austin","Corpus Christi","Dallas","El Paso","Fort Worth","Houston","Laredo","Plano","San Antonio")),
		array("Utah","/wp-content/uploads/shutterstock_254984635.jpg",array("Layton","Ogden","Orem","Provo","Salt Lake City","Sandy","South Jordan","St. George","West Jordan","West Valley City")),
		array("Vermont","/wp-content/uploads/shutterstock_125454413.jpg",array("Barre","Bellows Falls","Burlington","Essex Junction","Montpelier","Newport","Rutland","South Burlington","St. Albans","Winooski")),
		array("West Virginia","/wp-content/uploads/shutterstock_46815583.jpg",array("Beckley","Charleston","Clarksburg","Fairmont","Huntington","Martinsburg","Morgantown","Parkersburg","Weirton","Wheeling")),
		array("Virginia","/wp-content/uploads/shutterstock_46815583.jpg",array("Alexandria","Chesapeake","Hampton","Newport News","Norfolk","Portsmouth","Richmond","Roanoke","Suffolk","Virginia Beach")),
		array("Washington","/wp-content/uploads/shutterstock_233274136-min.jpg",array("Florida","Cape Coral","Fort Lauderdale","Hialeah","Jacksonville","Miami","Orlando","Port St. Lucie","St. Petersburg","Tallahassee")),
		array("Wisconsin","/wp-content/uploads/shutterstock_46815583.jpg",array("Appleton","Eau Claire","Green Bay","Janesville","Kenosha","Madison","Milwaukee","Oshkosh","Racine","Waukesha")),
		array("Wyoming","/wp-content/uploads/shutterstock_46815583.jpg",array("Casper","Cheyenne","Evanston","Gillette","Green River","Jackson","Laramie","Riverton","Rock Springs","Sheridan"))
	);

	//search using list in states array
	//first state found is the one assigned (just stops the loop quicker)
	//suggested update: order this list by state pages most commonly visited
	$total_states = count($states); $s=0;
	while ($state=="" && $s<>$total_states) {
		if(strpos($phrase,strtolower($states[$s][0]))!== false) {
			$state = trim($states[$s][0]);
			if($debug) $debug_notice .= "
	
	<!--
	////////////////////
	////////////////////
		
		State Found: ".$states[$s][0]."
					
	////////////////////
	////////////////////
	-->

			";
		}
		$s++;
	}
	$s=$s-1; //to step back for grabbing state banner
	$state_banner = rawurldecode($states[$s][1]);
	if(isset($states[$s][2])) $top_cities = $states[$s][2];
	else $top_cities="";







	//////////
	////  FIRST AND LAST WORDS
	/////////

	$first_word = substr($phrase,0,strpos($phrase," "));
	$last_word = substr($phrase,-1*(strlen($phrase)-strrpos($phrase," ")));


	



	//////////
	/// WORD SUBSTITUTIONS (pulled from PPC Plugin Settings)
	/////////

	//Some words in URLs are inappropriate to use as page titles. 
	//This replaces those based on what admins manually add to the PPC settings.
	//Requires the use of " => " --with those exact characters (one space on either side). It's used as a delimiter in an array below.
	//if it's blank, then disable the rest (unlikely situation)
	if(trim($frn_ppc_settings['find_replace'])!=="") {
		$find_replace_array = explode("\n",trim($frn_ppc_settings['find_replace'])); //creates array of each line in the settings textbox
		$i=0; //removed $word_found=false since we realized there were URL situations where we needed more than one replacement
		while ($i<(count($find_replace_array))) {
			if($find_replace_array[$i]!=="") {
				$find_replace_items = explode(" => ",$find_replace_array[$i]); //creates individual arrays of the find & replace phrases
				$find=strtolower($find_replace_items[0]);
				if(strpos($phrase,$find)!==false) {
					$replace=$find_replace_items[1];
					if($replace<>"") {
						$replaced_words=$replace;
						$to_replace=$find;
					}
					$phrase = str_replace($find,$replace,$phrase);
					//$word_found=true;
					if($debug) $debug_notice .= "
	
	<!--
	////////////////////
	////////////////////
	
		Find/Replace Found: ".$find_replace_items[0]."
	
	////////////////////
	////////////////////
	-->
	
	";
				}
			}
			$i++;
		} //loop
	}
	//changing to simpler to remember vars
	if(!isset($replaced_words)) $replaced_words="";
	if($replaced_words=="") {$find=""; $replace="";}
	else {
		$replace=$replaced_words;
		$find = $to_replace;
	}
	
	/*
	//get_bloginfo( 'wpurl' )
	//URI: $_SERVER["SERVER_NAME"]
	//Full URL: ".$_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"]."
	//Permalink: ".get_permalink(get_the_ID())."
	///Full list of vars and their values per stage of cleanup
	/*
	//Debugging by user login isn't working, so disabled for the time being for clarity purposes
	$debug_notice .= "
	<!--
	////////////////////
	////////////////////
		
		
		VARIABLES: 
			Domain: ".$domain."
			Domain No HTTP: ".str_replace("http://","",str_replace("https://","",get_bloginfo( 'wpurl' )))."
			Server Name: 
			Full URL: ".home_url( $wp->request )."
			Stripped URL: ".$pageURL."
			First Slash Pos: ".strpos($pageURL,"/")."
			Extracted Title: ".$ext_title."
			Cleaned Title: ".str_replace("-"," ",$ext_title)."
			Totals Chars: ".strlen($phrase_cleaned)."
			Second Word Start: ".strrpos($phrase_cleaned," ")."
			
			Phrase: ".$phrase."
			Manual Words Found (if any): ".$manual_words."
			State Found (if any): ".$state."
			First Word (if any): ".$first_word."
			Last Word (if any): ".$last_word."
		
		
	////////////////////
	////////////////////
	-->
	";
	// Begin to Remove theme CSS & Javascript
	// ben's function to remove all css

	function remove_all_theme_styles() {
	    global $wp_styles;
	    $wp_styles->queue = array();
	}
	add_action('wp_enqueue_scripts', 'remove_all_theme_styles', 1000);

	// ben's function to remove all javascript
	function remove_all_scripts() {
	    global $wp_scripts;
	    $wp_scripts->queue = array();
	}
	add_action( 'wp_enqueue_scripts', 'remove_all_scripts', 1000 );
	// End Remove theme CSS & Javascript

	function landing_page_styles() {
		global $wp_styles;
		wp_enqueue_style( 'mobile-opt', get_template_directory_uri() . '/css/layouts/mobile-opt.css', array(), '', 'all' );
		wp_enqueue_style( 'bxslider', get_template_directory_uri() . '/mobile-js/jquery.bxslider.css', array(), '', 'all' );
	}
	add_action( 'wp_enqueue_scripts', 'landing_page_styles', 1000 );
	// End Adding Landing Page CSS
	*/
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>

    <title><?php echo titleCase(get_the_title()); //define in the PPC plugin ?></title>

	<meta charset="<?php bloginfo( 'charset' ); ?>" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="description" content="Foundations Recovery Network">
	<meta name="robots" content="noindex, nofollow">

    <?php wp_head(); ?>
<?=$debug_notice; //displays all vars and values for the various cleanup stages ?>
<?php if(is_user_logged_in()) : ?>
	<style>
		div#wpadminbar {
		    display: none;
		}
		html {
		    margin-top: 0 !important;
		}
		nav.desktop-menu {
			top: 32px;
		}
	</style>
<?php endif; ?>
    
 <!-- script for yahoo added by bw on 2/12/18 -->
<script type="application/javascript">(function(w,d,t,r,u){w[u]=w[u]||[];w[u].push({'projectId':'10000','properties':{'pixelId':'10036079'}});var s=d.createElement(t);s.src=r;s.async=true;s.onload=s.onreadystatechange=function(){var y,rs=this.readyState,c=w[u];if(rs&&rs!="complete"&&rs!="loaded"){return}try{y=YAHOO.ywa.I13N.fireBeacon;w[u]=[];w[u].push=function(p){y([p])};y(c)}catch(e){}};var scr=d.getElementsByTagName(t)[0],par=scr.parentNode;par.insertBefore(s,scr)})(window,document,"script","https://s.yimg.com/wi/ytc.js","dotq");</script>

 <!-- scripts for bing, added by bw on 2/12/18 -->
<script>(function(w,d,t,r,u){var f,n,i;w[u]=w[u]||[],f=function(){var o={ti:"5996387"};o.q=w[u],w[u]=new UET(o),w[u].push("pageLoad")},n=d.createElement(t),n.src=r,n.async=1,n.onload=n.onreadystatechange=function(){var s=this.readyState;s&&s!=="loaded"&&s!=="complete"||(f(),n.onload=n.onreadystatechange=null)},i=d.getElementsByTagName(t)[0],i.parentNode.insertBefore(n,i)})(window,document,"script","//bat.bing.com/bat.js","uetq");</script><noscript><img src="//bat.bing.com/action/0?ti=5996387&Ver=2" height="0" width="0" style="display:none; visibility: hidden;" /></noscript>

<script>
window.uetq = window.uetq || []; 
window.uetq.push
({ 'ec':'Inbound Call', 'ea':'Call', 'el':'Phone Calls'}); 
</script>
   
<!-- Start Visual Website Optimizer Synchronous Code -->
	<script type='text/javascript'>
	var _vis_opt_account_id = 341821;
	var _vis_opt_protocol = (('https:' == document.location.protocol) ? 'https://' : 'http://');
	document.write('<s' + 'cript src="' + _vis_opt_protocol +
	'dev.visualwebsiteoptimizer.com/deploy/js_visitor_settings.php?v=1&a='+_vis_opt_account_id+'&url='
	+encodeURIComponent(document.URL)+'&random='+Math.random()+'" type="text/javascript">' + '<\/s' + 'cript>');
	</script>

	<script type='text/javascript'>
	if(typeof(_vis_opt_settings_loaded) == "boolean") { document.write('<s' + 'cript src="' + _vis_opt_protocol +
	'd5phz18u4wuww.cloudfront.net/vis_opt.js" type="text/javascript">' + '<\/s' + 'cript>'); }
	/* if your site already has jQuery 1.4.2, replace vis_opt.js with vis_opt_no_jquery.js above */
	</script>

	<script type='text/javascript'>
	if(typeof(_vis_opt_settings_loaded) == "boolean" && typeof(_vis_opt_top_initialize) == "function") {
			_vis_opt_top_initialize(); vwo_$(document).ready(function() { _vis_opt_bottom_initialize(); });
	}
	</script>
<!-- End Visual Website Optimizer Synchronous Code -->  
    
</head>
<body>

	<?php echo get_template_part( 'landing-parts/landing', 'header'); // this grabs the header bar ?>

	<div class="splash-container" style="background: linear-gradient(-30deg, rgba(33, 54, 74, 0.75) 50%, rgba(255, 255, 255, 0) 83%), url('<?=$state_banner; ?>' ); background-position: 50% 50%;">
		<div class="splash">
			<h1 class="splash-head"><?php echo titleCase(get_the_title()); //define in the PPC plugin ?></h1>
			<p class="splash-subhead">Foundations Recovery Network has provided the best treatment for <?php if($state=="") echo "[none]";else echo $state; ?> residents for over 15 years.</p>
			<hr class="line">
			<p class="splash-subhead">The path to recovery begins with just a simple phone call.</p>
			<h2 class="green-cta"><?php echo do_shortcode('[frn_phone number="(877) 345-3221" action="Phone Clicks in Landing Page Header"]'); ?></h2>
		</div>
	</div>

	<div class="content-wrapper">
	<div class="content">
		<section class="we-can-help">
			<a href="#our-process" class="smoothscroll"><button class="fa fa-caret-down"><p><span id="wch-blue">We Can Help.</span> Learn More About How.</p></button></a>
		</section>

		<section class="our-process" id="our-process">
			
			<div class="is-center">
				<h2>Our Process</h2>
				<p>We keep the path to recovery simple.</p>
			</div> 

			<div class="pure-g greybg op-svgs">
				<div class="pure-u-1 pure-u-md-1-3">
					<a href="#program-details" class="smoothscroll">
					<img src="/wp-content/themes/Rehab-And-Treatment/mobile-images/1-treatment-plan.svg">
					</a>
					<p class="is-center paddingcentered">We believe your situation and needs are unique, so your treatment plan should be too. Our experienced admissions coordinators will help you select a one-of-a-kind, tailor-made treatment plan specifically designed for you or your loved one.</p>
					<a href="#program-details" class="smoothscroll"><button class="OPButtons">Learn More</button></a>
				</div>

				<div class="pure-u-1 pure-u-md-1-3">
						<a href="#our-locations" class="smoothscroll">
							<img src="/wp-content/themes/Rehab-And-Treatment/mobile-images/2-best-location.svg">
						</a>
						<p class="is-center paddingcentered">We are the industry’s premier network of state and national treatment centers that has proudly served the residents of <?php if($state=="") echo "[none]";else echo $state; ?>  for over 15 years. Because we offer a wide selection of locations, you’ll be able to select one that is the best fit.</p>
						<a href="#our-locations" class="smoothscroll"><button class="OPButtons">Learn More</button></a>
				</div>

				<div class="pure-u-1 pure-u-md-1-3">
					<a href="#expect-call" class="smoothscroll">
						<img src="/wp-content/themes/Rehab-And-Treatment/mobile-images/3-schedule-admission.svg">
					</a>
						<p class="is-center paddingcentered">When you contact us, one of our knowledgeable admissions coordinators will assist you with all the details of scheduling an admission (health insurance verification, travel arrangements, etc.), making sure the transition into treatment is smooth and hassle-free.</p>
						<a href="#expect-call" class="smoothscroll"><button class="OPButtons">Learn More</button></a>
				</div>
			</div>

		</section>


		<div id="modal">
			  <div class="modal-content">
			    <div class="header">
			      <h2>Addictive Behavior</h2>
			    </div>
			    <div class="copy">
			      <p>When substance abuse becomes a part of a person’s life, addictive behavior can become destructive to that person and their loved ones. Our integrated treatment model compassionately treats the addiction to and abuse of alcohol and drugs.</p>
			      <a href="#integrated-treatment">Close</a> </div>
			  </div>
			  <div class="overlay"></div>
		</div>

		<div id="modal2">
			  <div class="modal2-content">
			    <div class="header2">
			      <h2>Mental Health Issue</h2>
			    </div>
			    <div class="copy">
			      <p>Many people who struggle with addiction have underlying mental health issues. One cannot expect addictive behavior to stop without addressing these issues, like depression, anxiety, bipolar disorder, post-traumatic stress disorder, and many others.</p>
			      <a href="#integrated-treatment">Close</a> </div>
			  </div>
			  <div class="overlay"></div>
		</div>

		<?php echo get_template_part( 'landing-parts/landing', 'integrated'); // integrated treatment section?>
		<?php echo get_template_part( 'landing-parts/landing', 'assessment'); // assessment section ?>
		<section class="our-locations" id="our-locations">
		<div class="inner">
			<div class="pure-g">
				<div class="pure-u-1 pure-u-md-1-1 pure-u-lg-1-2">
					<h2>Nationwide Locations</h2>
					<p>We are one of the largest networks of treatment centers in the country, so our clients from <?php if($state=="") echo "[none]";else echo $state; ?>  have a wide selection of locations from which to choose. Some prefer to find a place close to home, while others choose to leave their current environments completely so they can begin healing in a fresh setting.</p>
					<p>The most important part of choosing the right place for treatment is finding a place customized around your unique needs, which is our specialty. And if for some reason you feel our locations aren’t a good fit, we can help you get connected to other treatment centers because of our trusted referral relationships. Ultimately, you or loved one will be taken care of in a place of safety and healing.</p>
					<!--<div class="locationstabs" style="display:none;">
						<h6><span>Popular Cities we serve in <?php if($state=="") echo "[none]";else echo $state; ?>  <i class="fa fa-plus fa-lg"></i></span></h6>
						<h6><span>Counties we serve in <?php if($state=="") echo "[none]";else echo $state; ?>  <i class="fa fa-plus fa-lg"></i></span></h6>
					</div>-->
				</div>
				
				<div class="pure-u-1 pure-u-md-1-1 pure-u-lg-1-2">
				<h3 class="is-center">Let us help find the best location that meets your individual needs</h3>
				<div class="divider"></div>
					<span class="location--phone is-center"><?php echo do_shortcode('[frn_phone number="(877) 345-3221" action="Phone Clicks in Location Section"]'); ?></span>
				</div><!-- end column -->
			</div>
	</div><!-- end inner -->
		</section>
		<?php echo get_template_part( 'landing-parts/landing', 'standard'); // gold standard section ?>
	
	

		

		<section class="big-stats">
			<div class="pure-g is-center">
				<div class="pure-u-1 pure-u-md-1-3">
					<h3><span class="big-digits">80%</span><br><span class="bd-subtitle"> RECOVERY RATE</span></h3> <p>vs. 30% national avg. of abstinence after 1 year</p>
				</div>
				<div class="pure-u-1 pure-u-md-1-3">
					<h3><span class="big-digits">10,000</span><br><span class="bd-subtitle"> CLIENTS TREATED</span></h3> <p>Over 10,000 recovery stories, more each day</p>
				</div>
				<div class="pure-u-1 pure-u-md-1-3">
					<h3><span class="big-digits">1,250</span><br><span class="bd-subtitle"> CONNECTED TREATMENT CENTERS</span></h3> <p>Hundreds of locations for <?php if($state=="") echo "[none]";else echo $state; ?> residents</p>
				</div>
			</div>
		</section>

		<section class="cost-of-treatment greybg" id="cost-of-treatment">
			<div class="inner">
				<h2 class="is-center">Cost of Treatment</h2>
				<p class="is-center">Our top priority is to get you or your loved one healthy again, so we make sure the worries of cost and payment don’t get in the way. Instead of a bottom-line price for treatment, we calculate a payment plan realistic for you and your family by looking at:</p>
				<div class="cot-list">
					<ul>
						<li><i class="fa fa-check" aria-hidden="true"></i> Your financial situation</li>
						<li><i class="fa fa-check" aria-hidden="true"></i> Any insurance coverage</li>
						<li><i class="fa fa-check" aria-hidden="true"></i> Your specific medical conditions</li>
						<li><i class="fa fa-check" aria-hidden="true"></i> Your unique treatment needs</li>
						<li><i class="fa fa-check" aria-hidden="true"></i> Many other factors</li>
					</ul>
					<p class="ta-l"><strong><i>You can rest assured that our seasoned admissions team will exhaust every option to get you or your loved one into treatment hassle-free and affordably.</i></strong></p>
				</div>
			</div>
		</section>

		<section class="insurance">
			<h2 class="is-center">We work with all the major health insurance providers…</h2>
			<hr>
			<div class="pure-g i-icons">
				<div class="pure-u-1-2 pure-u-md-1-5">
					<img src="/wp-content/themes/Rehab-And-Treatment/mobile-images/insurance-icons/aetna_large1.png">
				</div>
				<div class="pure-u-1-2 pure-u-md-1-5">
					<img src="/wp-content/themes/Rehab-And-Treatment/mobile-images/insurance-icons/bcbshield_large1.png">
				</div>
				<div class="pure-u-1-2 pure-u-md-1-5">
					<img src="/wp-content/themes/Rehab-And-Treatment/mobile-images/insurance-icons/communityhealthalliance1.png">
				</div>
				<div class="pure-u-1-2 pure-u-md-1-5">
					<img src="/wp-content/themes/Rehab-And-Treatment/mobile-images/insurance-icons/humana_large1.png">
				</div>
				<div class="pure-u-1-2 pure-u-md-1-5">
					<img src="/wp-content/themes/Rehab-And-Treatment/mobile-images/insurance-icons/magellanhealth_large1.png">
				</div>
				<div class="pure-u-1-2 pure-u-md-1-5">
					<img src="/wp-content/themes/Rehab-And-Treatment/mobile-images/insurance-icons/multiplan_Large1.png">
				</div>
				<div class="pure-u-1-2 pure-u-md-1-5">
					<img src="/wp-content/themes/Rehab-And-Treatment/mobile-images/insurance-icons/POMCO.png">
				</div>
				<div class="pure-u-1-2 pure-u-md-1-5">
					<img src="/wp-content/themes/Rehab-And-Treatment/mobile-images/insurance-icons/UMR.png">
				</div>
				<div class="pure-u-1-2 pure-u-md-1-5">
					<img src="/wp-content/themes/Rehab-And-Treatment/mobile-images/insurance-icons/coresource-compressed.png">
				</div>
				<div class="pure-u-1-2 pure-u-md-1-5">
					<img src="/wp-content/themes/Rehab-And-Treatment/mobile-images/insurance-icons/valueoptions_large1-150x34.png">
				</div>
			</div>
			<hr>
			<h2 class="is-center">…and many more!</h2>
			<div class="l-box is-center"><p><i><strong>BONUS: </strong>Our dedicated benefits verification team handles all the details with your health insurance provider so you don’t have to! Currently, we don’t accept clients with Medicare or Medicaid into our inpatient facilities, but we do have the ability to refer out to other locations that can accept that kind of coverage.</i></p>
			</div>
		</section>

		<section class="program-details" id="program-details"> 
			<h2 class="is-center">Program Details</h2>
			<p class="is-center">We give our clients a vast array of treatment options.</p>
			<div class="accordian-wrapper">
				<div class="accordian">		
				<ul>
				  <li>
				    <input type="checkbox">
				    <i></i>
				    <h3>A Safe &amp; Proven Medical Approach</h3>
						<p>&bull; Integrated Treatment (substance addiction &amp;  mental health issues)</p>
						<p>&bull; Compassionate, medically-monitored detox</p>
						<p>&bull; Round-the-Clock care with a personal therapist</p>
						<p>&bull; Physician-Directed medication management</p>
						<p>&bull; Treatment for eating disorders, sex addiction and more</p>
				  </li>
				  <li>
				    <input type="checkbox" checked>
				    <i></i>
				    <h3>Education &amp; Specialized Services</h3>
		    			<p>&bull; Diverse groups like healthy boundaries, anger management, etc.</p>
						<p>&bull; Prevention education and recovery coaching to avoid future relapse</p>
						<p>&bull; Dual recovery self-help education</p>
						<p>&bull; Sober escort and travel support services</p>
						<p>&bull; Professional interventions</p>
						<p>&bull; Helpful alternative sentencing and pretrial intervention</p>
				  </li>
				  <li>
				    <input type="checkbox" checked>
				    <i></i>
				    <h3>Community Family Therapy</h3>
		    			<p>&bull; Monthly “family weekends”</p>
						<p>&bull; Relief and recovery programs for chronic pain and addiction</p>
						<p>&bull; Specialized program for the LGBT community</p>
						<p>&bull; Supportive, active and empowering nationwide alumni program</p>
				  </li>
				  <li>
				    <input type="checkbox" checked>
				    <i></i>
				    <h3>A Variety of Therapy Options</h3>	
						<p>&bull; Yoga and massage therapy</p>
						<p>&bull; Expressive arts and equine therapy</p>
						<p>&bull; On-site ropes course</p>
						<p>&bull; Fitness and wellness facilities</p>
						<p>&bull; Outdoor adventure therapy and sober fun activities</p>
				  </li>
				</ul>
				</div>
			</div>
		</section>

		<section class="testimonials">
			<h2 class="is-center">What Our Clients Say</h2>

			<div id="slider1" class="csslider infinity">
				  <input type="radio" name="slides" checked="checked" id="slides_1"/>
				  <input type="radio" name="slides" id="slides_2"/>
				  <input type="radio" name="slides" id="slides_3"/>
				  <input type="radio" name="slides" id="slides_4"/>
				  <input type="radio" name="slides" id="slides_5"/>
				  <input type="radio" name="slides" id="slides_6"/>
				  <input type="radio" name="slides" id="slides_7"/>
				  <input type="radio" name="slides" id="slides_8"/>
				  <ul>
				    <li class="slide"><img src="/wp-content/themes/Rehab-And-Treatment/mobile-images/testimonials/RichardS.jpg"><blockquote class="testimonial-quote">My favorite part of the program was the individualized treatment and the personal relationships that I had with the therapists, counselors and staff. At any given moment if I needed to sit down with anyone, they were there to talk. It didn’t matter what time of day it was.<br><cite>Richard S.</cite></blockquote></li>
				    <li class="slide"><img src="/wp-content/themes/Rehab-And-Treatment/mobile-images/testimonials/VanessaW.jpg"><blockquote class="testimonial-quote">The person I was when I entered treatment was very dark. I didn’t think that they would get me, that they would understand what it was that I was going through. So, from the beginning I had a rough time opening up to them. By the end of my stay, they were friends.<br><cite>Vanessa W.</cite></blockquote></li>
				    <li class="slide"><img src="/wp-content/themes/Rehab-And-Treatment/mobile-images/testimonials/BrentW.jpg"><blockquote class="testimonial-quote">My life was centered around my addiction. If I hadn't gotten the help that I needed at Foundations, I would have been dead or held up in my parents' home living a miserable existence.<br><cite>Brent W.</cite></blockquote></li>
				    <li class="slide"><img src="/wp-content/themes/Rehab-And-Treatment/mobile-images/testimonials/MarkS.jpg"><blockquote class="testimonial-quote">My parents feared for my life. Things were getting out of control. If someone would have told me that it would have been as easy as picking up the phone for Foundations, I would definitely pick up the phone and call. It's the best thing I could have ever done.<br><cite>Mark S.</cite></blockquote></li>
				    <li class="slide"><img src="/wp-content/themes/Rehab-And-Treatment/mobile-images/testimonials/CristinaL.jpg"><blockquote class="testimonial-quote">The treatment plan was customized for my needs. As my needs changed, the program changed for what I needed it to be. This program is about giving yourself the chance and the opportunity to become all those things that you don’t think you could ever become. Just give yourself the chance.<br><cite>Christina L.</cite></blockquote></li>
				    <li class="slide"><img src="/wp-content/themes/Rehab-And-Treatment/mobile-images/testimonials/JanetM.jpg"><blockquote class="testimonial-quote">This is a place that’s so nurturing, that’s going to help you get through the problems you have to get through. They’re not just telling you how to get better. They’re showing you and giving you life lessons. They gave me my spirit back.<br><cite>Janet M.</cite></blockquote></li>
				   <li class="slide"><img src="/wp-content/themes/Rehab-And-Treatment/mobile-images/testimonials/JenH.jpg"><blockquote class="testimonial-quote">Foundations isn’t just a detox center. They take special care to try to understand what those things are that make a person use in the first place and address them, giving you very personalized care. At the most desperate point in your life, Foundations will be there. There is hope.<br><cite>Jen H.</cite></blockquote></li>
				    <li class="slide"><img src="/wp-content/themes/Rehab-And-Treatment/mobile-images/testimonials/MarthaF.jpg"><blockquote class="testimonial-quote">Before my son went to Foundations, there were people who said to me that he was hopeless. But that just wasn’t true. Nothing else worked until we found Foundations.<br><cite>Martha F.</cite></blockquote></li>
				  </ul>
				  <div class="arrows">
				    <label for="slides_1"></label>
				    <label for="slides_2"></label>
				    <label for="slides_3"></label>
				    <label for="slides_4"></label>
				    <label for="slides_5"></label>
				    <label for="slides_6"></label>
				    <label for="slides_7"></label>
				    <label for="slides_8"></label>
				    <label for="slides_1" class="goto-first"></label>
				    <label for="slides_8" class="goto-last"></label>
				  </div>
				  <div class="navigation"> 
				    <div>
				      <label for="slides_1"></label>
				      <label for="slides_2"></label>
				      <label for="slides_3"></label>
				      <label for="slides_4"></label>
				      <label for="slides_5"></label>
				      <label for="slides_6"></label>
				      <label for="slides_7"></label>
				      <label for="slides_8"></label>
				    </div>
				  </div>
			</div>

			
		</section>

		<section class="testimonials desktop-tc mobile-hide">
			<h2 class="is-center">What Our Clients Say</h2>

			<div class="gallery items-3">

				<div id="item-1" class="control-operator"></div>
				<div id="item-2" class="control-operator"></div>

				<div class="secondary-controls">
					<div class="superfluous">
						<nav>
							<a class="arrow-previous" href="#item-1"></a>
							<a class="arrow-next" href="#item-2"></a>
						</nav>
					</div>
				</div>
		
				<figure class="item">
					<img src="/wp-content/themes/Rehab-And-Treatment/mobile-images/testimonials/RichardS.jpg"><blockquote class="testimonial-quote">My favorite part of the program was the individualized treatment and the personal relationships that I had with the therapists, counselors and staff. At any given moment if I needed to sit down with anyone, they were there to talk. It didn’t matter what time of day it was.<br><cite>Richard S.</cite></blockquote>
					<img src="/wp-content/themes/Rehab-And-Treatment/mobile-images/testimonials/VanessaW.jpg"><blockquote class="testimonial-quote">The person I was when I entered treatment was very dark. I didn’t think that they would get me, that they would understand what it was that I was going through. So, from the beginning I had a rough time opening up to them. By the end of my stay, they were friends.<br><cite>Vanessa W.</cite></blockquote>
					<div style="clear:both; padding:2em;"></div>
					<img src="/wp-content/themes/Rehab-And-Treatment/mobile-images/testimonials/BrentW.jpg"><blockquote class="testimonial-quote">My life was centered around my addiction. If I hadn't gotten the help that I needed at Foundations, I would have been dead or held up in my parents' home living a miserable existence.<br><cite>Brent W.</cite></blockquote>
					<img src="/wp-content/themes/Rehab-And-Treatment/mobile-images/testimonials/MarkS.jpg"><blockquote class="testimonial-quote">My parents feared for my life. Things were getting out of control. If someone would have told me that it would have been as easy as picking up the phone for Foundations, I would definitely pick up the phone and call. It's the best thing I could have ever done.<br><cite>Mark S.</cite></blockquote>
				</figure>

				<figure class="item">
					<img src="/wp-content/themes/Rehab-And-Treatment/mobile-images/testimonials/CristinaL.jpg"><blockquote class="testimonial-quote">The treatment plan was customized for my needs. As my needs changed, the program changed for what I needed it to be. This program is about giving yourself the chance and the opportunity to become all those things that you don’t think you could ever become. Just give yourself the chance.<br><cite>Christina L.</cite></blockquote>
					<img src="/wp-content/themes/Rehab-And-Treatment/mobile-images/testimonials/JanetM.jpg"><blockquote class="testimonial-quote">This is a place that’s so nurturing, that’s going to help you get through the problems you have to get through. They’re not just telling you how to get better. They’re showing you and giving you life lessons. They gave me my spirit back.<br><cite>Janet M.</cite></blockquote>
					<div style="clear:both; padding:2em;"></div>
					<img src="/wp-content/themes/Rehab-And-Treatment/mobile-images/testimonials/JenH.jpg"><blockquote class="testimonial-quote">Foundations isn’t just a detox center. They take special care to try to understand what those things are that make a person use in the first place and address them, giving you very personalized care. At the most desperate point in your life, Foundations will be there. There is hope.<br><cite>Jen H.</cite></blockquote>
					<img src="/wp-content/themes/Rehab-And-Treatment/mobile-images/testimonials/MarthaF.jpg"><blockquote class="testimonial-quote">Before my son went to Foundations, there were people who said to me that he was hopeless. But that just wasn’t true. Nothing else worked until we found Foundations.<br><cite>Martha F.</cite></blockquote>
				</figure>

				<div class="controls">
					<a href="#item-1" class="control-button">•</a>
					<a href="#item-2" class="control-button">•</a>
				</div>

			</div>
		</section>

		<section class="expect-call greybg" id="select-your-plan">
			<h2 class="is-center">What to Expect When You Call</h2>
			<div class="pure-g">
				<div class="pure-u-1-1 pure-u-md-3-5">
					<div class="video-container">
						<iframe src="https://www.youtube.com/embed/0slPGPtP6F8?rel=0&amp;showinfo=0" frameborder="0" allowfullscreen></iframe>
					</div>
				</div>
				<div class="pure-u-1-1 pure-u-md-2-5">
					<p class="paddingcentered is-center">Many people can be hesitant to pick up the phone and call. We get that. Fortunately, our track record proves that our clients are relieved after they decide to dial our number. Because we cover ALL the details most important to them, their minds are put at ease and they can rest easy knowing everything will be taken care of.</p>
					<p class="paddingcentered is-center">When you contact us, you will be connected to an experienced admissions coordinator who will help you start the process by selecting the best treatment plan for you or your loved one.</p>
				</div>
			</div>
		</section>

		<section class="cta-phone" id="expect-call">
			<h3 class="is-center">The path to recovery begins with just a simple phone call.</h3>
			<h2 class="is-center green-cta"><?php echo do_shortcode('[frn_phone number="(877) 345-3221" action="Phone Clicks in Landing Page CTA Above Footer"]'); ?></h2>
			<p class="is-center">Give us a call.</p>
		</section>

		<!-- SCROLL TO TOP BUTTON -->
		<a href="#scrolltotop" class="scrollToTop"><i class="fa fa-caret-up"></i></a>

		<link rel='stylesheet' id='mobile-opt-css'  href='/wp-content/themes/Rehab-And-Treatment/css/layouts/mobile-opt.css?ver=4.8.0' type='text/css' media='all' />
		<?php if(function_exists('frn_enqueue_script')) : //verifies that the FRN Plugin is activated ?>
		<link rel='stylesheet' id='frn_plugin'  href='<?=plugins_url('frn_plugins/frn_plugin.css');?>?v=1.12' type='text/css' media='all' />
		<?php endif; ?>
		<script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>
			
		<?php wp_footer(); ?>
		<?php /*  
		//Dax commented this out via PHP instead of HTML comments to keep HTML cleaner 10/18/17
		<footer>
			<img class="pure-img-responsive" src="/wp-content/themes/Rehab-And-Treatment/mobile-images/FRN-logo-dark.png">
			<p>Copyright © 1995-2016 Foundations Recovery Network. All Rights Reserved. | Confidential and Private Call: <strong><?php echo do_shortcode('[frn_phone number="(877) 345-3221" action="Phone Clicks in Landing Page Footer"]'); ?></strong> | Privacy Policy</p>
		</footer>
		<?php */ ?>
		
	</div><!-- end content -->
	</div><!-- end content-wrapper -->

	<?php /* ?>
	//Dax removed 10/18/17 to rely more on the FRN Plugin v2.11.1
		<!-- #####
		IfByPHone Phone Number Code
		##### -->
		<script type="text/javascript">
			var _stk = "29b7c6a9512f3b1450d85acb9aa55fb8818ba6aa";

			(function(){
				var a=document, b=a.createElement("script"); b.type="text/javascript";
				b.async=!0; b.src=('https:'==document.location.protocol ? 'https://' :
				'http://') + 'd31y97ze264gaa.cloudfront.net/assets/st/js/st.js';
				a=a.getElementsByTagName("script")[0]; a.parentNode.insertBefore(b,a);
			})();
		</script>
		<!-- #####
		End IfByPhone
		##### -->

			<!-- #####
		
		##### -->
	<?php */ 
	global $frn_mobile;
    if($frn_mobile=="Smartphone") : 
    //Used by Dax on 10/18/17 to remove this code since it's unnecessary for desktop/ipad users ?>
    
	<!-- BEGIN SCROLL TO TOP BUTTON -->
	<script type="text/javascript">
	$(document).ready(function(){
		//Check to see if the window is top if not then display button
		$(window).scroll(function(){
			if ($(this).scrollTop() > 100) {
				$('.scrollToTop').fadeIn();
			} else {
				$('.scrollToTop').fadeOut();
			}
		});
		//Click event to scroll to top
		$('.scrollToTop').click(function(){
			$('html, body').animate({scrollTop : 0},800);
			return false;
		});			
	});
	</script>
	<!--  END SCROLL TO TOP BUTTON -->
	<?php endif; ?>

	<!--  BEGIN FLOATING FOOTER BAR -->
	<script type="text/javascript">
	$(function() {
	    //caches a jQuery object containing the header element
	    var header = $("#mobile-nav");
	    $(window).scroll(function() {
	        var scroll = $(window).scrollTop();

	        if (scroll >= 80) {
	            header.removeClass('hidden2').addClass("visible");
	        } else {
	            header.removeClass("visible").addClass('hidden2');
	        }
	    });
	});
	</script>
	<!--  END FLOATING FOOTER BAR -->

	<!--  BEGIN SMOOTH SCROLL -->
	<script type="text/javascript">
	$(function() {
	  $('a.smoothscroll[href*="#"]:not([href=""])').click(function() {
	    if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
	      var target = $(this.hash);
	      target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
	      if (target.length) {
	        $('html, body').animate({
	          scrollTop: target.offset().top - 70
	        }, 1100);
	        return false;
	      }
	    }
	  });
	});
	</script>
	<!--  END SMOOTH SCROLL -->

	<script type="application/javascript">
	window.dotq = window.dotq || [];
	window.dotq.push(
	{
	 'projectId': '10000',
	 'properties': {
	   'pixelId': '10036079',
	   'qstrings': {
	     'et': 'custom',
	     'ec': 'inbound call',
	     'ea': 'call',
	     'el': 'phone calls',
	     'ev': 'event value',
	     'gv': 'conversion value'
	  }
	} } ); </script>
	<?php /* // Dax removed 10/18/17 to rely more on FRN Plugin v2.11.1 ?><script src="https://cdn.optimizely.com/js/1033590360.js"></script><?php */ ?>
	

	<link rel="stylesheet" href="http://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css">
	<link href='https://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
	<link href='https://fonts.googleapis.com/css?family=Source+Sans+Pro:400,400i,600i,700' rel='stylesheet' type='text/css'>

</body>
</html>