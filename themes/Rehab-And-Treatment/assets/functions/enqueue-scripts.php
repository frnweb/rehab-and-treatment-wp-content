<?php
function site_scripts() {
  global $wp_styles; // Call global $wp_styles variable to add conditional wrapper around ie stylesheet the WordPress way
  global $wp_query, $post;
    
  if ( $wp_query->post_count == 0 ) {

    global $wp_scripts;
    $wp_styles->queue = array();
    $wp_scripts->queue = array();
    
    //wp_enqueue_style( 'mobile-opt', get_template_directory_uri() . '/css/layouts/mobile-opt.css', array(), '', 'all' );
    add_filter( 'feed_links_show_comments_feed', '__return_false' );
    add_filter( 'emoji_svg_url', '__return_false' );
    
    // all actions related to emojis
    remove_action( 'admin_print_styles', 'print_emoji_styles' );
    remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
    remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
    remove_action( 'wp_print_styles', 'print_emoji_styles' );
    remove_filter( 'wp_mail', 'wp_staticize_emoji_for_email' );
    remove_filter( 'the_content_feed', 'wp_staticize_emoji' );
    remove_filter( 'comment_text_rss', 'wp_staticize_emoji' );

    // filter to remove TinyMCE emojis
    add_filter( 'tiny_mce_plugins', 'disable_emojicons_tinymce' );

    frn_plugin_controls();

  }

 elseif ( is_page_template( 'template-outpatient.php' ) ) { 

    global $wp_scripts;
    $wp_styles->queue = array();
    $wp_scripts->queue = array();

    add_filter( 'feed_links_show_comments_feed', '__return_false' );
    add_filter( 'emoji_svg_url', '__return_false' );
                // all actions related to emojis
    remove_action( 'admin_print_styles', 'print_emoji_styles' );
    remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
    remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
    remove_action( 'wp_print_styles', 'print_emoji_styles' );
    remove_filter( 'wp_mail', 'wp_staticize_emoji_for_email' );
    remove_filter( 'the_content_feed', 'wp_staticize_emoji' );
    remove_filter( 'comment_text_rss', 'wp_staticize_emoji' );

    // filter to remove TinyMCE emojis
    add_filter( 'tiny_mce_plugins', 'disable_emojicons_tinymce' );

    frn_plugin_controls();

  }

  else {

    //NORMAL PAGES ON THE SITE (NO LANDING PAGES)

    // Load What-Input files in footer
    wp_enqueue_script( 'what-input', get_template_directory_uri() . '/vendor/what-input/what-input.min.js', array(), '', true );
    
    // Adding Foundation scripts file in the footer
    wp_enqueue_script( 'foundation-js', get_template_directory_uri() . '/vendor/foundation-sites/dist/foundation.min.js', array( 'jquery' ), '6.2.3', true );
    
    // Adding scripts file in the footer
    wp_enqueue_script( 'site-js', get_template_directory_uri() . '/assets/js/scripts.js', array( 'jquery' ), '', true );
    
     // Register Motion-UI
    wp_enqueue_style( 'motion-ui-css', get_template_directory_uri() . '/vendor/motion-ui/dist/motion-ui.min.css', array(), '', 'all' );
	
	// Select which grid system you want to use (Foundation Grid by default)
    wp_enqueue_style( 'foundation-css', get_template_directory_uri() . '/vendor/foundation-sites/dist/foundation.min.css', array(), '', 'all' );
     //wp_enqueue_style( 'foundation-css', get_template_directory_uri() . '/vendor/foundation-sites/dist/foundation-flex.min.css', array(), '', 'all' );

    // Register main stylesheet
    wp_enqueue_style( 'site-css', get_template_directory_uri() . '/assets/css/style.css', array(), '', 'all' );

    // Comment reply script for threaded comments
    if ( is_singular() AND comments_open() AND (get_option('thread_comments') == 1)) {
      wp_enqueue_script( 'comment-reply' );
    }

  }
}
add_action('wp_enqueue_scripts', 'site_scripts', 999);



function frn_plugin_controls() {

    if(function_exists('frn_enqueue_script')) : //verifies that the FRN Plugin is activated

        //FRN Plugin registers the script, but doesn't display it. 
        // This gets the frn_plugin.js file to load
        wp_enqueue_script( 'frn_plugin_scripts' );
        wp_dequeue_script('jquery');

    endif;
}