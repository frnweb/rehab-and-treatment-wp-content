		<section class="testimonials desktop-tc mobile-no ">
			<h2 class="is-center">Our Staff</h2>

		
			  <figure class="item our-staff-1">
			  		<div>
					<img src="/wp-content/themes/Rehab-And-Treatment/mobile-images/nashville-staff/Amanda-Elkin-Director.png"><p class="name">Amanda Elkin</p><p>Director</p>
					</div>
					<div>
					<img src="/wp-content/themes/Rehab-And-Treatment/mobile-images/nashville-staff/Patrick-Foshee-Business-Development.png"><p class="name">Patrick Foshee</p><p>Business Development</p>
					</div>
			  </figure>

			</div>
			
		
	</section>

	<section class="testimonials chicago-staff mobile-only">
		
					<h2 class="is-center">Our Staff</h2>

			<div class="gallery items-3">
  				<div id="item-30" class="control-operator"></div>
  				<div id="item-31" class="control-operator"></div>
  				<div id="item-32" class="control-operator"></div>
  				<div id="item-33" class="control-operator"></div>
  				<div id="item-34" class="control-operator"></div>
  				<div id="item-35" class="control-operator"></div>
  				<div id="item-36" class="control-operator"></div>
  				<div id="item-37" class="control-operator"></div>
  				<div id="item-38" class="control-operator"></div>

  				<div class="secondary-controls">
					<div class="superfluous">
					</div>
				</div>
		
			  <figure class="item our-staff-1">
			  		<div>
			  		<a class="arrow-previous staff" href="#item-31"></a>
					<img src="/wp-content/themes/Rehab-And-Treatment/mobile-images/nashville-staff/Amanda-Elkin-Director.png"><p class="name">Amanda Elkin</p><p>Director</p>
					<a class="arrow-next staff" href="#item-31"></a>
					</div>
			  </figure>
			  <figure class="item our-staff-1">
					<div>
					<a class="arrow-previous staff" href="#item-30"></a>
					<img src="/wp-content/themes/Rehab-And-Treatment/mobile-images/nashville-staff/Patrick-Foshee-Business-Development.png"><p class="name">Patrick Foshee</p><p>Business Development</p>
					<a class="arrow-next staff" href="#item-30"></a>
					</div>
			  </figure>

			  <div class="controls">
			    <a href="#item-30" class="control-button">•</a>
			    <a href="#item-31" class="control-button">•</a>
			  </div>
			</div>
		
	</section>

<style type="text/css">

	figure.item.our-staff-1{
		text-align: center;
	}
		
	@media (min-width:1020px) {
		section.testimonials.chicago-staff.mobile-only{
			display: none !important
		}
		section.testimonials.desktop-tc.mobile-no{
			display: block !important;
			padding: 2em 2em 4em;
		}
	}

	@media (max-width:1019px) {
		section.testimonials.chicago-staff.mobile-only{
			display: block !important;
			padding-bottom: 4em;
		}
		a.arrow-next.staff {
	    	margin-top: -195px !important;
	    }
	    a.arrow-previous.staff {
	    	margin-top: 55px !important;
	    }
		section.testimonials.desktop-tc.mobile-no{
			display: none !important;
		}
	}

</style>