		<section class="testimonials desktop-tc mobile-no ">
			<h2 class="is-center">Our Staff</h2>

			<div class="gallery items-3">
  				<div id="item-3" class="control-operator"></div>
  				<div id="item-4" class="control-operator"></div>

  				<div class="secondary-controls">
					<div class="superfluous">
						<nav>
							<a class="arrow-previous staff" href="#item-3"></a>
							<a class="arrow-next staff" href="#item-4"></a>
						</nav>
					</div>
				</div>
		
			  <figure class="item our-staff-1">
			  		<div>
					<img src="/wp-content/themes/Rehab-And-Treatment/mobile-images/memphis-staff/Britney-Simmons-Outpatient-Therapist.png"><p class="name">Britney Simmons</p><p>Outpatient Therapist</p>
					</div>
					<div>
					<img src="/wp-content/themes/Rehab-And-Treatment/mobile-images/memphis-staff/Brittney-Jackson-Primary-Therapist.png"><p class="name">Brittney Jackson</p><p>Primary Therapist</p>
					</div>
					<div>
					<img src="/wp-content/themes/Rehab-And-Treatment/mobile-images/memphis-staff/Greg-Hughes-Director.png"><p class="name">Greg Hughes</p><p>Director</p>
					</div>
					<div>
					<img src="/wp-content/themes/Rehab-And-Treatment/mobile-images/memphis-staff/Jennifer-Weaver-Assessment-Coordinator.png"><p class="name">Jennifer Weaver</p><p>Assessment Coordinator</p>
					</div>
			  </figure>

			  <figure class="item our-staff-1">
			  		<div>
					<img src="/wp-content/themes/Rehab-And-Treatment/mobile-images/memphis-staff/Loretta-Parnell-Residential-Counselor.png"><p class="name">Loretta Parnell</p><p>Residential Counselor</p>
					</div>
					<div>
					<img src="/wp-content/themes/Rehab-And-Treatment/mobile-images/memphis-staff/Rachel-Cody-Business-Office-Manager.png"><p class="name">Rachel Cody</p><p>Business Office Manager</p>
					</div>
			  </figure>

			  <div class="controls">
			    <a href="#item-3" class="control-button">•</a>
			    <a href="#item-4" class="control-button">•</a>
			  </div>
			</div>
			
		
	</section>

	<section class="testimonials chicago-staff mobile-only">
		
					<h2 class="is-center">Our Staff</h2>

			<div class="gallery items-3">
  				<div id="item-30" class="control-operator"></div>
  				<div id="item-31" class="control-operator"></div>
  				<div id="item-32" class="control-operator"></div>
  				<div id="item-33" class="control-operator"></div>
  				<div id="item-34" class="control-operator"></div>
  				<div id="item-35" class="control-operator"></div>

  				<div class="secondary-controls">
					<div class="superfluous">
					</div>
				</div>
		
			  <figure class="item our-staff-1">
			  		<div>
			  		<a class="arrow-previous staff" href="#item-37"></a>
					<img src="/wp-content/themes/Rehab-And-Treatment/mobile-images/memphis-staff/Britney-Simmons-Outpatient-Therapist.png"><p class="name">Britney Simmons</p><p>Outpatient Therapist</p>
					<a class="arrow-next staff" href="#item-31"></a>
					</div>
			  </figure>
			  <figure class="item our-staff-1">
					<div>
					<a class="arrow-previous staff" href="#item-30"></a>
					<img src="/wp-content/themes/Rehab-And-Treatment/mobile-images/memphis-staff/Brittney-Jackson-Primary-Therapist.png"><p class="name">Brittney Jackson</p><p>Primary Therapist</p>
					<a class="arrow-next staff" href="#item-32"></a>
					</div>
			  </figure>
			  <figure class="item our-staff-1">
					<div>
					<a class="arrow-previous staff" href="#item-31"></a>
					<img src="/wp-content/themes/Rehab-And-Treatment/mobile-images/memphis-staff/Greg-Hughes-Director.png"><p class="name">Greg Hughes</p><p>Director</p>
					<a class="arrow-next staff" href="#item-33"></a>
					</div>
			  </figure>
			  <figure class="item our-staff-1">
					<div>
					<a class="arrow-previous staff" href="#item-32"></a>
					<img src="/wp-content/themes/Rehab-And-Treatment/mobile-images/memphis-staff/Jennifer-Weaver-Assessment-Coordinator.png"><p class="name">Jennifer Weaver</p><p>Assessment Coordinator</p>
					<a class="arrow-next staff" href="#item-34"></a>
					</div>
			  </figure>
			  <figure class="item our-staff-1">
					<div>
					<a class="arrow-previous staff" href="#item-33"></a>
					<img src="/wp-content/themes/Rehab-And-Treatment/mobile-images/memphis-staff/Loretta-Parnell-Residential-Counselor.png"><p class="name">Loretta Parnell</p><p>Residential Counselor</p>
					<a class="arrow-next staff" href="#item-35"></a>
					</div>
			  </figure>

			  <figure class="item our-staff-1">
			  		<div>
			  		<a class="arrow-previous staff" href="#item-34"></a>
					<img src="/wp-content/themes/Rehab-And-Treatment/mobile-images/memphis-staff/Rachel-Cody-Business-Office-Manager.png"><p class="name">Rachel Cody</p><p>Business Office Manager</p>
					<a class="arrow-next staff" href="#item-30"></a>
					</div>
				</figure>
			

			  <div class="controls">
			    <a href="#item-30" class="control-button">•</a>
			    <a href="#item-31" class="control-button">•</a>
			    <a href="#item-32" class="control-button">•</a>
			    <a href="#item-33" class="control-button">•</a>
			    <a href="#item-34" class="control-button">•</a>
			    <a href="#item-35" class="control-button">•</a>
			  </div>
			</div>
		
	</section>

<style type="text/css">
		
	@media (min-width:1020px) {
		section.testimonials.chicago-staff.mobile-only{
			display: none !important
		}
		section.testimonials.desktop-tc.mobile-no{
			display: block !important;
			padding: 2em 2em 4em;
		}
	}

	@media (max-width:1019px) {
		section.testimonials.chicago-staff.mobile-only{
			display: block !important;
			padding-bottom: 4em;
		}
		a.arrow-next.staff {
	    	margin-top: -195px !important;
	    }
	    a.arrow-previous.staff {
	    	margin-top: 55px !important;
	    }
		section.testimonials.desktop-tc.mobile-no{
			display: none !important;
		}
	}

</style>