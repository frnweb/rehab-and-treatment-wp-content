		<section class="testimonials desktop-tc mobile-no ">
			<h2 class="is-center">Our Staff</h2>

			<div class="gallery items-3">
  				<div id="item-3" class="control-operator"></div>
  				<div id="item-4" class="control-operator"></div>

  				<div class="secondary-controls">
					<div class="superfluous">
						<nav>
							<a class="arrow-previous staff" href="#item-3"></a>
							<a class="arrow-next staff" href="#item-4"></a>
						</nav>
					</div>
				</div>
		
			  <figure class="item our-staff-1">
			  		<div>
					<img src="/wp-content/themes/Rehab-And-Treatment/mobile-images/detroit-staff/Carol-Auiler-Primary-Therapist.png"><p class="name">Carol Auiler</p><p>Primary Therapist</p>
					</div>
					<div>
					<img src="/wp-content/themes/Rehab-And-Treatment/mobile-images/detroit-staff/Carolynn-Metz-Clinical-Services-Manager.png"><p class="name">Carolynn Metz</p><p>Clinical Services Manager</p>
					</div>
					<div>
					<img src="/wp-content/themes/Rehab-And-Treatment/mobile-images/detroit-staff/Dr-Kirchoff-Medical-Director.png"><p class="name">Dr. Kirchoff</p><p>Medical Director</p>
					</div>
					<div>
					<img src="/wp-content/themes/Rehab-And-Treatment/mobile-images/detroit-staff/Mike-Ferriter-Primary-Therapist.png"><p class="name">Mike Ferriter</p><p>Primary Therapist</p>
					</div>
			  </figure>

			  <figure class="item our-staff-1">
			  		<div>
					<img src="/wp-content/themes/Rehab-And-Treatment/mobile-images/detroit-staff/Dr-Theadia-Carey-Psychiatrist.png"><p class="name">Dr. Theadia Carey</p><p>Psychiatrist</p>
					</div>
					<div>
					<img src="/wp-content/themes/Rehab-And-Treatment/mobile-images/detroit-staff/Justine-Belvitch-Business-Office-Manager.png"><p class="name">Justine Belvitch</p><p>Business Office Manager</p>
					</div>
					<div>
					<img src="/wp-content/themes/Rehab-And-Treatment/mobile-images/detroit-staff/Leslie-Baker-Nurse.png"><p class="name">Leslie Baker</p><p>Nurse</p>
					</div>
					<div>
					<img src="/wp-content/themes/Rehab-And-Treatment/mobile-images/detroit-staff/Sarah-Bono-Business-Development-Representative.png"><p class="name">Sarah Bono</p><p>Business Development Representative</p>
					</div>
			  </figure>

			  <div class="controls">
			    <a href="#item-3" class="control-button">•</a>
			    <a href="#item-4" class="control-button">•</a>
			  </div>
			</div>
			
		
	</section>

	<section class="testimonials chicago-staff mobile-only">
		
					<h2 class="is-center">Our Staff</h2>

			<div class="gallery items-3">
  				<div id="item-30" class="control-operator"></div>
  				<div id="item-31" class="control-operator"></div>
  				<div id="item-32" class="control-operator"></div>
  				<div id="item-33" class="control-operator"></div>
  				<div id="item-34" class="control-operator"></div>
  				<div id="item-35" class="control-operator"></div>
  				<div id="item-36" class="control-operator"></div>
  				<div id="item-37" class="control-operator"></div>
  				<div id="item-38" class="control-operator"></div>

  				<div class="secondary-controls">
					<div class="superfluous">
					</div>
				</div>
		
			  <figure class="item our-staff-1">
			  		<div>
			  		<a class="arrow-previous staff" href="#item-37"></a>
					<img src="/wp-content/themes/Rehab-And-Treatment/mobile-images/detroit-staff/Carol-Auiler-Primary-Therapist.png"><p class="name">Carol Auiler</p><p>Primary Therapist</p>
					<a class="arrow-next staff" href="#item-31"></a>
					</div>
			  </figure>
			  <figure class="item our-staff-1">
					<div>
					<a class="arrow-previous staff" href="#item-30"></a>
					<img src="/wp-content/themes/Rehab-And-Treatment/mobile-images/detroit-staff/Carolynn-Metz-Clinical-Services-Manager.png"><p class="name">Carolynn Metz</p><p>Clinical Services Manager</p>
					<a class="arrow-next staff" href="#item-32"></a>
					</div>
			  </figure>
			  <figure class="item our-staff-1">
					<div>
					<a class="arrow-previous staff" href="#item-31"></a>
					<img src="/wp-content/themes/Rehab-And-Treatment/mobile-images/detroit-staff/Dr-Kirchoff-Medical-Director.png"><p class="name">Dr. Kirchoff</p><p>Medical Director</p>
					<a class="arrow-next staff" href="#item-33"></a>
					</div>
			  </figure>
			  <figure class="item our-staff-1">
					<div>
					<a class="arrow-previous staff" href="#item-32"></a>
					<img src="/wp-content/themes/Rehab-And-Treatment/mobile-images/detroit-staff/Mike-Ferriter-Primary-Therapist.png"><p class="name">Mike Ferriter</p><p>Primary Therapist</p>
					<a class="arrow-next staff" href="#item-34"></a>
					</div>
			  </figure>

			  <figure class="item our-staff-1">
			  		<div>
			  		<a class="arrow-previous staff" href="#item-33"></a>
					<img src="/wp-content/themes/Rehab-And-Treatment/mobile-images/detroit-staff/Dr-Theadia-Carey-Psychiatrist.png"><p class="name">Dr. Theadia Carey</p><p>Psychiatrist</p>
					<a class="arrow-next staff" href="#item-35"></a>
					</div>
				</figure>
				<figure class="item our-staff-1">
					<div>
					<a class="arrow-previous staff" href="#item-34"></a>
					<img src="/wp-content/themes/Rehab-And-Treatment/mobile-images/detroit-staff/Justine-Belvitch-Business-Office-Manager.png"><p class="name">Justine Belvitch</p><p>Business Office Manager</p>
					<a class="arrow-next staff" href="#item-36"></a>
					</div>
				</figure>
				<figure class="item our-staff-1">
					<div>
					<a class="arrow-previous staff" href="#item-35"></a>
					<img src="/wp-content/themes/Rehab-And-Treatment/mobile-images/detroit-staff/Leslie-Baker-Nurse.png"><p class="name">Leslie Baker</p><p>Nurse</p>
					<a class="arrow-next staff" href="#item-37"></a>
					</div>
				</figure>
				<figure class="item our-staff-1">
					<div>
					<a class="arrow-previous staff" href="#item-36"></a>
					<img src="/wp-content/themes/Rehab-And-Treatment/mobile-images/detroit-staff/Sarah-Bono-Business-Development-Representative.png"><p class="name">Sarah Bono</p><p>Business Development Representative</p>
					<a class="arrow-next staff" href="#item-30"></a>
					</div>
			  </figure>

			  <div class="controls">
			    <a href="#item-30" class="control-button">•</a>
			    <a href="#item-31" class="control-button">•</a>
			    <a href="#item-32" class="control-button">•</a>
			    <a href="#item-33" class="control-button">•</a>
			    <a href="#item-34" class="control-button">•</a>
			    <a href="#item-35" class="control-button">•</a>
			    <a href="#item-36" class="control-button">•</a>
			    <a href="#item-37" class="control-button">•</a>
			  </div>
			</div>
		
	</section>

<style type="text/css">
		
	@media (min-width:1020px) {
		section.testimonials.chicago-staff.mobile-only{
			display: none !important
		}
		section.testimonials.desktop-tc.mobile-no{
			display: block !important;
			padding: 2em 2em 4em;
		}
	}

	@media (max-width:1019px) {
		section.testimonials.chicago-staff.mobile-only{
			display: block !important;
			padding-bottom: 4em;
		}
		a.arrow-next.staff {
	    	margin-top: -195px !important;
	    }
	    a.arrow-previous.staff {
	    	margin-top: 55px !important;
	    }
		section.testimonials.desktop-tc.mobile-no{
			display: none !important;
		}
	}

</style>