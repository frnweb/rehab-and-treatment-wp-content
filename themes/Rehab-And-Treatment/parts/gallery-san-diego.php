		<section class="testimonials desktop-tc mobile-no ">
			<h2 class="is-center">Photo Gallery</h2>

			<div class="gallery items-4">
  				<div id="item-5" class="control-operator"></div>
  				<div id="item-6" class="control-operator"></div>

  				<div class="secondary-controls">
					<div class="superfluous">
						<nav>
							<a class="arrow-previous" href="#item-5"></a>
							<a class="arrow-next" href="#item-6"></a>
						</nav>
					</div>
				</div>
		
			  <figure class="item our-staff-1">
			  		<div>
					<img src="/wp-content/themes/Rehab-And-Treatment/mobile-images/san-diego-gallery/Foundations-San-Diego-01.jpg">
					</div>
					<div>
					<img src="/wp-content/themes/Rehab-And-Treatment/mobile-images/san-diego-gallery/Foundations-San-Diego-03.jpg">
					</div>
					<div>
					<img src="/wp-content/themes/Rehab-And-Treatment/mobile-images/san-diego-gallery/Foundations-San-Diego-04.jpg">
					</div>
					<div>
					<img src="/wp-content/themes/Rehab-And-Treatment/mobile-images/san-diego-gallery/Foundations-San-Diego-05.jpg">
					</div>
			  </figure>

			  <figure class="item our-staff-1">
			  		<div>
					<img src="/wp-content/themes/Rehab-And-Treatment/mobile-images/san-diego-gallery/Foundations-San-Diego-12.jpg">
					</div>
					<div>
					<img src="/wp-content/themes/Rehab-And-Treatment/mobile-images/san-diego-gallery/Foundations-San-Diego-14.jpg">
					</div>
					<div>
					<img src="/wp-content/themes/Rehab-And-Treatment/mobile-images/san-diego-gallery/Foundations-San-Diego-15.jpg">
					</div>
					<div>
					<img src="/wp-content/themes/Rehab-And-Treatment/mobile-images/san-diego-gallery/Foundations-San-Diego-17.jpg">
					</div>
			  </figure>

			  <div class="controls">
			    <a href="#item-5" class="control-button">•</a>
			    <a href="#item-6" class="control-button">•</a>
			  </div>
			</div>
			
		
	</section>

	<section class="testimonials chicago-staff mobile-only">
		
	  <h2 class="is-center">Photo Gallery</h2>

			<div class="gallery items-4">
  				<div id="item-40" class="control-operator"></div>
  				<div id="item-41" class="control-operator"></div>
  				<div id="item-42" class="control-operator"></div>
  				<div id="item-43" class="control-operator"></div>
  				<div id="item-44" class="control-operator"></div>
  				<div id="item-45" class="control-operator"></div>
  				<div id="item-46" class="control-operator"></div>
  				<div id="item-47" class="control-operator"></div>
  				<div id="item-48" class="control-operator"></div>

  				<div class="secondary-controls">
					<div class="superfluous">
					</div>
				</div>
		
			  <figure class="item our-staff-1">
			  		<div>
			  		<a class="arrow-previous" href="#item-47"></a>
					<img src="/wp-content/themes/Rehab-And-Treatment/mobile-images/san-diego-gallery/Foundations-San-Diego-01.jpg">
					<a class="arrow-next" href="#item-41"></a>
					</div>
			  </figure>
			  <figure class="item our-staff-1">
					<div>
					<a class="arrow-previous" href="#item-30"></a>
					<img src="/wp-content/themes/Rehab-And-Treatment/mobile-images/san-diego-gallery/Foundations-San-Diego-03.jpg">
					<a class="arrow-next" href="#item-42"></a>
					</div>
			  </figure>
			  <figure class="item our-staff-1">
					<div>
					<a class="arrow-previous" href="#item-41"></a>
					<img src="/wp-content/themes/Rehab-And-Treatment/mobile-images/san-diego-gallery/Foundations-San-Diego-04.jpg">
					<a class="arrow-next" href="#item-43"></a>
					</div>
			  </figure>
			  <figure class="item our-staff-1">
					<div>
					<a class="arrow-previous" href="#item-42"></a>
					<img src="/wp-content/themes/Rehab-And-Treatment/mobile-images/san-diego-gallery/Foundations-San-Diego-05.jpg">
					<a class="arrow-next" href="#item-44"></a>
					</div>
			  </figure>

			  <figure class="item our-staff-1">
			  		<div>
			  		<a class="arrow-previous" href="#item-43"></a>
					<img src="/wp-content/themes/Rehab-And-Treatment/mobile-images/san-diego-gallery/Foundations-San-Diego-12.jpg">
					<a class="arrow-next" href="#item-45"></a>
					</div>
				</figure>
				<figure class="item our-staff-1">
					<div>
					<a class="arrow-previous" href="#item-44"></a>
					<img src="/wp-content/themes/Rehab-And-Treatment/mobile-images/san-diego-gallery/Foundations-San-Diego-14.jpg">
					<a class="arrow-next" href="#item-46"></a>
					</div>
				</figure>
				<figure class="item our-staff-1">
					<div>
					<a class="arrow-previous" href="#item-45"></a>
					<img src="/wp-content/themes/Rehab-And-Treatment/mobile-images/san-diego-gallery/Foundations-San-Diego-15.jpg">
					<a class="arrow-next" href="#item-47"></a>
					</div>
				</figure>
				<figure class="item our-staff-1">
					<div>
					<a class="arrow-previous" href="#item-46"></a>
					<img src="/wp-content/themes/Rehab-And-Treatment/mobile-images/san-diego-gallery/Foundations-San-Diego-17.jpg">
					<a class="arrow-next" href="#item-40"></a>
					</div>
			  </figure>

			  <div class="controls">
			    <a href="#item-40" class="control-button">•</a>
			    <a href="#item-41" class="control-button">•</a>
			    <a href="#item-42" class="control-button">•</a>
			    <a href="#item-43" class="control-button">•</a>
			    <a href="#item-44" class="control-button">•</a>
			    <a href="#item-45" class="control-button">•</a>
			    <a href="#item-46" class="control-button">•</a>
			    <a href="#item-47" class="control-button">•</a>
			  </div>
			</div>
		
	</section>

<style type="text/css">
		
	@media (min-width:1020px) {
		section.testimonials.san-diego-staff.mobile-only{
			display: none !important
		}
		section.testimonials.desktop-tc.mobile-no{
			display: block !important;
			padding: 2em 2em 4em;
		}
	}

	@media (max-width:1019px) {
		section.testimonials.san-diego-staff.mobile-only{
			display: block !important;
			padding-bottom: 4em;
		}
		.arrow-next {
	    	margin-top: -140px !important;
    		right: 0px !important;
	    }
	    .arrow-previous{
	    	margin-top: 65px !important;
    		left: 0px !important;
	    }
		section.testimonials.desktop-tc.mobile-no{
			display: none !important;
		}
		.photo-gallery figure.item.our-staff-1 {
    		height: 225px;
		}
	}
		@media (min-width: 300px) and (max-width: 340px) {
		.arrow-next {
	    	margin-top: -90px !important;
	    	}
		}
		@media (min-width: 341px) and (max-width: 399px) {
		.arrow-next {
	    	margin-top: -125px !important;
	    	}
		}
	section.photo-gallery, .gallery.items-4{
		max-width: 1600px !important;
	}
</style>