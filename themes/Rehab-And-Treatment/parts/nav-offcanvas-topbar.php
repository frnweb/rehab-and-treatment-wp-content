<!-- By default, this menu will use off-canvas for small
	 and a topbar for medium-up -->

<nav class="desktop-menu">
<div class="top-bar" id="top-bar-menu" >
	<div class="top-bar-left float-left">
		<ul class="menu">
			<li><a href="<?php echo home_url(); ?>"><img src="http://www.rehabandtreatment.com/wp-content/themes/Rehab-And-Treatment/mobile-images/foundations-1.svg"></a></li>
		</ul>
	</div>

	
	<div class="top-bar-right show-for-medium" >
		<?php joints_top_nav(); ?>	
	</div>
	
	<div class="top-bar-right float-right show-for-small-only">
		<ul class="menu">
			<!-- <li><button class="menu-icon" type="button" data-toggle="off-canvas"></button></li> -->
			<li><a data-toggle="off-canvas"><?php _e( 'MENU', 'jointswp' ); ?></a></li>
		</ul>
	</div>
</div>
</nav>