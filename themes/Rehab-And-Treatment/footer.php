					<?php /* 
					//Dax removed this 10/18/17 to clean up the HTML
					?>
					<footer class="footer" role="contentinfo">
						<div id="inner-footer" class="row">
							<div class="large-12 medium-12 columns">
								<nav role="navigation">
		    						<?php joints_footer_links(); ?>
		    					</nav>
		    				</div>
						</div> <!-- end #inner-footer -->
					</footer> <!-- end .footer -->
					<?php */ ?>
				</div>  <!-- end .main-content -->
			</div> <!-- end .off-canvas-wrapper-inner -->
		</div> <!-- end .off-canvas-wrapper -->

		<?php wp_footer(); ?>

		<!--  BEGIN FLOATING FOOTER BAR -->
		<script type="text/javascript">
			$(function() {
			    //caches a jQuery object containing the header element
			    var header = $("#mobile-nav");
			    $(window).scroll(function() {
			        var scroll = $(window).scrollTop();

			        if (scroll >= 40) {
			            header.removeClass('hidden2').addClass("visible");
			        } else {
			            header.removeClass("visible").addClass('hidden2');
			        }
			    });
			});
		</script>
		<!--  END FLOATING FOOTER BAR -->
		
	</body>
</html> <!-- end page -->