<?php
/*
Template Name: Outpatient PPC
*/
?>

<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>

	<?php /* //removed 10/24/17 since it's a duplicate to what is already provided by plugins ?>
    <title><?php echo titleCase(get_the_title()); //defined in the PPC plugin ?></title>
    <?php */ 
	?><meta charset="<?php bloginfo( 'charset' ); ?>" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="description" content="Foundations Recovery Network">
	<meta name="robots" content="noindex, nofollow">

    <?php wp_head(); ?>
<?php if(is_user_logged_in()) : ?>
	<style>
		div#wpadminbar {
		    display: none;
		}
		html {
		    margin-top: 0 !important;
		}
		nav.desktop-menu {
			top: 32px;
		}
	</style>
<?php endif; ?>
    
</head>
<body>

<div class="header">
	<nav class="home-menu pure-menu pure-menu-horizontal pure-menu-fixed" id="frn-nav">
	    <div class="inner">
	    	<a class="pure-menu-heading" href=""><img src="/wp-content/themes/Rehab-And-Treatment/mobile-images/foundations-1.svg">
	     	</a>
	   		 <ul class="pure-menu-list mobile-hide">
	        	<li class="pure-menu-item"><strong><a href="#who-we-are" class="pure-menu-link smoothscroll">Who We Are</a></strong></li>
	        	<li class="pure-menu-item"><strong><a href="#our-process" class="pure-menu-link smoothscroll">Our Process</a></strong></li>
	        	<li class="pure-menu-item"><strong><a href="#our-locations" class="pure-menu-link smoothscroll">Locations</a></strong></li>
	        	<li class="pure-menu-item"><strong><a href="#program-details" class="pure-menu-link smoothscroll">Programs</a></strong></li>
	        	<li class="pure-menu-item"><strong><a href="#cost-of-treatment" class="pure-menu-link smoothscroll">Cost of Treatment</a></strong></li>
	   		 </ul>
	    </div>
    </nav>
    <div id="mobile-nav">
    	<div class="pure-g">
			<div class="pure-u-1-5 pure-u-md-1-3"><?php 
					echo do_shortcode('[lhn_inpage button="email" text="empty" desktop="Email" class="fa fa-envelope" category="Contact Options Bar" action="Email Clicks in Outpatient Landing Page Menu Bar" ]'); 
					/* 
					// Dax commented this out via PHP instead of HTML to keep HTML cleaner 10/19/17
					?><a onclick="window.open('http://www.livehelpnow.net/lhn/TicketsVisitor.aspx?lhnid=14160','Ticket','left=' + (screen.width - 550-32) / 2 + ',top=50,scrollbars=yes,menubar=no,height=550,width=450,resizable=yes,toolbar=no,location=no,status=no');return false;"><i class="fa fa-envelope" aria-hidden="true"></i><span class="mobile-hide">Email</span></a><?php */ 
				?></div>

			<div class="pure-u-3-5 number pure-u-md-1-3"><i class="fa fa-mobile mobile-hide" aria-hidden="true"></i><span class="phone-number-fixed"><?php echo do_shortcode('[frn_phone action="Phone Clicks in Outpatient Landing Page Menu Bar"]'); ?></span></div>

			<div class="pure-u-1-5 pure-u-md-1-3"><?php 
					echo do_shortcode('[lhn_inpage button="chat" text="empty" desktop="Live Chat" offline_mobile="empty" class="fa fa-comments-o" category="Contact Options Bar" action="Chat Clicks in Outpatient Landing Page Menu Bar" ]'); 
						/* 
						// Dax commented this out via PHP instead of HTML to keep HTML cleaner 10/18/17
						?><a onclick="OpenLHNChat();return false; ga('send', 'event', 'Contact Options Flyout', 'Chat/Email');"><i class="fa fa-comments-o" aria-hidden="true"></i><span class="mobile-hide">Live Chat</span><?php */ 
				?></div>
	</div>
</div><!-- end header -->


<div class="splash-container" style="background: linear-gradient(-30deg, rgba(247, 148, 29, 0.5) 50%, rgba(149, 149, 149, 0.4) 83%), url('<?php echo get_field('header_image'); ?>' ); background-position: 50% 65%;">
	<div class="splash">
		<h1 class="splash-head"><?php echo get_field('header_oppc'); ?></h1>
		<h2 class="splash-subhead"><?php echo get_field('header_subtitle_oppc'); ?></h2>
	</div>
</div>


<div class="content-wrapper">
<div class="content">
	<section class="who-we-are" id="who-we-are">
		<h2 class="is-center">Who We Are</h2>
		<p class="is-center"><?php echo get_field('who_we_are_copy'); ?></p>
		<div class="pure-g">
			<div class="pure-u-1-1 video">
				<div class="video-container">
					<iframe src="https://www.youtube.com/embed/<?php echo get_field('youtube_id'); ?>?rel=0&amp;showinfo=0" frameborder="0" allowfullscreen></iframe>
				</div>
			</div>
	</section>

	<section class="ceo-message greybg">
		
		<div class="pure-g">
			<h2>A Message From Our <?php echo get_field('ceo_title'); ?></h2>
			<hr>
			<div class="pure-u-1-1 pure-u-md-3-5">
				<p class="paddingcentered"><?php echo get_field('ceo_message'); ?></p>
			</div>
			<div class="pure-u-1-1 pure-u-md-2-5">
			<img src="<?php echo get_field('ceo_image'); ?>" style="display: block;margin: 0 auto;">
			</div>
		</div>

	</section>

	<section class="our-staff">

		<?php if( get_field('staff_gallery') == 'chicago' ): ?>
			<?php get_template_part( 'parts/staff', 'chicago' ); ?>
		<?php elseif ( get_field('staff_gallery') == 'detroit' ): ?>
			<?php get_template_part( 'parts/staff', 'detroit' ); ?>
		<?php elseif ( get_field('staff_gallery') == 'san-diego' ): ?>
			<?php get_template_part( 'parts/staff', 'san-diego' ); ?>
		<?php elseif ( get_field('staff_gallery') == 'san-francisco' ): ?>
			<?php get_template_part( 'parts/staff', 'san-francisco' ); ?>
		<?php elseif ( get_field('staff_gallery') == 'la-encino' ): ?>
			<?php get_template_part( 'parts/staff', 'la-encino' ); ?>
		<?php elseif ( get_field('staff_gallery') == 'the-canyon' ): ?>
			<?php get_template_part( 'parts/staff', 'the-canyon' ); ?>
		<?php elseif ( get_field('staff_gallery') == 'atl-roswell' ): ?>
			<?php get_template_part( 'parts/staff', 'atl-roswell' ); ?>
		<?php elseif ( get_field('staff_gallery') == 'atl-midtown' ): ?>
			<?php get_template_part( 'parts/staff', 'atl-midtown' ); ?>
		<?php elseif ( get_field('staff_gallery') == 'nashville' ): ?>
			<?php get_template_part( 'parts/staff', 'nashville' ); ?>
		<?php elseif ( get_field('staff_gallery') == 'memphis' ): ?>
			<?php get_template_part( 'parts/staff', 'memphis' ); ?>
		<?php elseif ( get_field('staff_gallery') == 'michaels-house' ): ?>
			<?php get_template_part( 'parts/staff', 'michaels-house' ); ?>
		<?php elseif ( get_field('staff_gallery') == 'talbott-columbus' ): ?>
			<?php get_template_part( 'parts/staff', 'talbott-columbus' ); ?>
		<?php elseif ( get_field('staff_gallery') == 'talbott-dunwoody' ): ?>
			<?php get_template_part( 'parts/staff', 'talbott-dunwoody' ); ?>
		<?php endif; ?>

	</section>

	<section class="our-locations greybg" id="our-locations">
		<div class="pure-g">
			<div class="pure-u-1-1 pure-u-md-2-5">
				<h4>Our Locations</h4>
				<?php echo get_field('our_locations'); ?>
			</div>

			<div class="pure-u-1-1 pure-u-md-3-5">
				<style type="text/css">

					.acf-map {
						width: 100%;
						height: 400px;
						border: #ccc solid 1px;
						margin: 20px 0;
					}

					/* fixes potential theme css conflict */
					.acf-map img {
					   max-width: inherit !important;
					}

				</style>
				<script type='text/javascript' src='/wp-includes/js/jquery/jquery.js?ver=1.12.4'></script>
				<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCuGEkS3XOItflpYiLMK5ClgulNVI1l9TM"></script>
				<script type="text/javascript">
					(function($) {

					/*
					*  new_map
					*
					*  This function will render a Google Map onto the selected jQuery element
					*
					*  @type	function
					*  @date	8/11/2013
					*  @since	4.3.0
					*
					*  @param	$el (jQuery element)
					*  @return	n/a
					*/

					function new_map( $el ) {
						
						// var
						var $markers = $el.find('.marker');
						
						
						// vars
						var args = {
							zoom		: 16,
							center		: new google.maps.LatLng(0, 0),
							mapTypeId	: google.maps.MapTypeId.ROADMAP,
							scrollwheel:  false
						};
						
						
						// create map	        	
						var map = new google.maps.Map( $el[0], args);
						
						
						// add a markers reference
						map.markers = [];
						
						
						// add markers
						$markers.each(function(){
							
					    	add_marker( $(this), map );
							
						});
						
						
						// center map
						center_map( map );
						
						
						// return
						return map;
						
					}

					/*
					*  add_marker
					*
					*  This function will add a marker to the selected Google Map
					*
					*  @type	function
					*  @date	8/11/2013
					*  @since	4.3.0
					*
					*  @param	$marker (jQuery element)
					*  @param	map (Google Map object)
					*  @return	n/a
					*/

					function add_marker( $marker, map ) {

						// var
						var latlng = new google.maps.LatLng( $marker.attr('data-lat'), $marker.attr('data-lng') );

						// create marker
						var marker = new google.maps.Marker({
							position	: latlng,
							map			: map
						});

						// add to array
						map.markers.push( marker );

						// if marker contains HTML, add it to an infoWindow
						if( $marker.html() )
						{
							// create info window
							var infowindow = new google.maps.InfoWindow({
								content		: $marker.html()
							});

							// show info window when marker is clicked
							google.maps.event.addListener(marker, 'click', function() {

								infowindow.open( map, marker );

							});
						}

					}

					/*
					*  center_map
					*
					*  This function will center the map, showing all markers attached to this map
					*
					*  @type	function
					*  @date	8/11/2013
					*  @since	4.3.0
					*
					*  @param	map (Google Map object)
					*  @return	n/a
					*/

					function center_map( map ) {

						// vars
						var bounds = new google.maps.LatLngBounds();

						// loop through all markers and create bounds
						$.each( map.markers, function( i, marker ){

							var latlng = new google.maps.LatLng( marker.position.lat(), marker.position.lng() );

							bounds.extend( latlng );

						});

						// only 1 marker?
						if( map.markers.length == 1 )
						{
							// set center of map
						    map.setCenter( bounds.getCenter() );
						    map.setZoom( 16 );
						}
						else
						{
							// fit to bounds
							map.fitBounds( bounds );
							// popup is shown and map is not visible
					google.maps.event.trigger(map, 'resize');
						}

					}

					/*
					*  document ready
					*
					*  This function will render each map when the document is ready (page has loaded)
					*
					*  @type	function
					*  @date	8/11/2013
					*  @since	5.0.0
					*
					*  @param	n/a
					*  @return	n/a
					*/
					// global var
					var map = null;

					$(document).ready(function(){

						$('.acf-map').each(function(){

							// create map
							map = new_map( $(this) );

						});

					});

					})(jQuery);
				</script>
				<div class="acf-map">
					<div class="marker" data-lat="<?php echo get_field('map')['lat']; ?>" data-lng="<?php echo get_field('map')['lng']; ?>">
						<?php echo get_field('our_locations'); ?>
					</div>
				</div>
			</div>

	</section>

	<section class="photo-gallery">
		
		<?php if( get_field('staff_gallery') == 'chicago' ): ?>
			<?php get_template_part( 'parts/gallery', 'chicago' ); ?>
		<?php elseif ( get_field('staff_gallery') == 'detroit' ): ?>
			<?php get_template_part( 'parts/gallery', 'detroit' ); ?>
		<?php elseif ( get_field('staff_gallery') == 'san-diego' ): ?>
			<?php get_template_part( 'parts/gallery', 'san-diego' ); ?>
		<?php elseif ( get_field('staff_gallery') == 'san-francisco' ): ?>
			<?php get_template_part( 'parts/gallery', 'san-francisco' ); ?>
		<?php elseif ( get_field('staff_gallery') == 'la-encino' ): ?>
			<?php get_template_part( 'parts/gallery', 'la-encino' ); ?>
		<?php elseif ( get_field('staff_gallery') == 'the-canyon' ): ?>
			<?php get_template_part( 'parts/gallery', 'the-canyon' ); ?>
		<?php elseif ( get_field('staff_gallery') == 'atl-roswell' ): ?>
			<?php get_template_part( 'parts/gallery', 'atl-roswell' ); ?>
		<?php elseif ( get_field('staff_gallery') == 'atl-midtown' ): ?>
			<?php get_template_part( 'parts/gallery', 'atl-midtown' ); ?>
		<?php elseif ( get_field('staff_gallery') == 'nashville' ): ?>
			<?php get_template_part( 'parts/gallery', 'nashville' ); ?>
		<?php elseif ( get_field('staff_gallery') == 'memphis' ): ?>
			<?php get_template_part( 'parts/gallery', 'memphis' ); ?>
		<?php elseif ( get_field('staff_gallery') == 'michaels-house' ): ?>
			<?php get_template_part( 'parts/gallery', 'michaels-house' ); ?>
		<?php elseif ( get_field('staff_gallery') == 'talbott-columbus' ): ?>
			<?php get_template_part( 'parts/gallery', 'talbott-columbus' ); ?>
		<?php elseif ( get_field('staff_gallery') == 'talbott-dunwoody' ): ?>
			<?php get_template_part( 'parts/gallery', 'talbott-dunwoody' ); ?>
		<?php endif; ?>

	</section>

	<section class="expect-call greybg" id="select-your-plan">
		<h2 class="is-center">What to Expect When You Call</h2>
		<div class="pure-g video">
			<div class="pure-u-1-1 pure-u-md-3-5">
				<div class="video-container">
					<iframe src="https://www.youtube.com/embed/D_x7ea6_uDI?rel=0&amp;showinfo=0" frameborder="0" allowfullscreen></iframe>
				</div>
			</div>
			<div class="pure-u-1-1 pure-u-md-2-5">
				<p class="paddingcentered is-center">Many people can be hesitant to pick up the phone and call. We get that. Fortunately, our track record proves that our clients are relieved after they decide to dial our number. Because we cover ALL the details most important to them, their minds are put at ease and they can rest easy knowing everything will be taken care of.</p>
				<p class="paddingcentered is-center">When you contact us, you will be connected to an experienced admissions coordinator who will help you start the process by selecting the best treatment plan for you or your loved one.</p>
			</div>
		</div>

<section class="our-process" id="our-process">
		<div class="is-center">
			<h2>Our Process</h2>
			<p>We keep the path to recovery simple.</p>
		</div> 
			<div class="pure-g op-svgs">
				<div class="pure-u-1 pure-u-md-1-3">
					<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="226" height="226" viewBox="0 0 113 113">
  						<metadata><?xpacket begin="﻿" id="W5M0MpCehiHzreSzNTczkc9d"?>
						<x:xmpmeta xmlns:x="adobe:ns:meta/" x:xmptk="Adobe XMP Core 5.6-c138 79.159824, 2016/09/14-01:09:01        ">
   						<rdf:RDF xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
      					<rdf:Description rdf:about=""/>
   						</rdf:RDF>
						</x:xmpmeta>                           
							<?xpacket end="w"?></metadata>
							<image id="Chat" width="113" height="113" xlink:href="data:img/png;base64,iVBORw0KGgoAAAANSUhEUgAAAOIAAADiCAMAAABkzuvlAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAC/VBMVEU0W3o0W3o0W3o0W3o0W3o0W3o0W3o0W3o0W3o0W3o0W3o0W3o0W3o0W3o0W3o0W3o0W3o0W3o0W3o0W3o0W3o0W3o0W3o0W3o0W3o0W3o0W3o0W3o0W3o0W3o0W3o0W3o0W3o0W3o0W3o0W3o0W3o0W3o0W3o0W3o0W3o0W3o0W3o0W3o0W3o0W3o0W3o0W3o0W3o0W3o0W3o0W3o0W3o0W3o0W3o0W3o0W3o0W3o0W3o0W3o0W3o0W3o0W3o0W3o0W3o0W3o0W3o0W3o0W3o0W3o0W3o0W3o0W3o0W3o0W3o0W3o0W3o0W3o0W3o0W3o0W3o0W3o0W3o0W3o0W3o0W3o0W3o0W3o0W3o0W3o0W3o0W3o0W3o0W3o0W3o0W3o0W3o0W3o0W3o0W3o0W3o0W3o0W3o0W3o0W3o0W3o0W3o0W3o0W3o0W3o0W3o0W3o0W3o0W3o0W3o0W3o0W3o0W3o0W3o0W3o0W3o0W3o0W3o0W3o0W3o0W3o0W3o0W3o0W3o0W3o0W3o0W3o0W3o0W3o0W3o0W3o0W3o0W3o0W3o0W3o0W3o0W3o0W3o0W3o0W3o0W3o0W3o0W3o0W3o0W3o0W3o0W3o0W3o0W3o0W3o0W3o0W3o0W3o0W3o0W3o0W3o0W3o0W3o0W3o0W3o0W3o0W3o0W3o0W3o0W3o0W3o0W3o0W3o0W3o0W3o0W3o0W3o0W3o0W3o0W3o0W3o0W3o0W3o0W3o0W3o0W3o0W3o0W3o0W3o0W3o0W3o0W3o0W3o0W3o0W3o0W3o0W3o0W3o0W3o0W3o0W3o0W3o0W3o0W3o0W3o0W3o0W3o0W3o0W3o0W3o0W3o0W3o0W3o0W3o0W3o0W3o0W3o0W3o0W3o0W3o0W3o0W3o0W3o0W3o0W3o0W3o0W3o0W3o0W3o0W3o0W3o0W3o0W3o0W3o0W3o0W3o0W3o0W3o0W3o0W3o0W3o0W3o0W3o0W3o0W3o0W3o0W3o0W3o0W3o0W3o0W3o0W3o0W3o0W3oAAAAzIAlmAAAA/XRSTlMABChJaIajuMnb7fD76uDVuqSKc1gyCw9FeqvU+o9aIhVWmNjnrnUBMYHP5pxQDTCN4vmzCBZ28Zs/O6VmCQJTyhtgHtaVGDnM9H8a4VRr8rQfJsP45YhuXEs+LjRARFVqhZq2/L3QOAYrUr5PPNGMDAO3945s/gpM7hee0wcvPRN55EgcV2EkgBBwLRTaeCzr9R2tcd9eDk5Gwhn9KkPeEl8g7IPdBdwjl6igudfBzieLZ7v2bzesn7+TQSVtqcXp42Om0rB3mUpCx3u1UYJ0NlmQhHKhvDrv6PNdxnyvfcB+os2qYpQzaWXZp00py4kRyJ2RNVtHIcSWspKxm/M+0AAAAAFiS0dE/tIAwlMAAAAJcEhZcwAACxIAAAsSAdLdfvwAAAAHdElNRQfhAgMQABPhIXynAAASDklEQVR42t1deXxN1xa+MRMij2iUaINwVRAUMSVKawhijKA1C0IMSQ1pkIrpESViqraCCI3hUVPQoopSpQOlQ6p9rVa1OtPxvdfe3+/dnNwMa+3h7HPOPudEvj/vXXuv9d1z7lp7WHtth8MgvMqULVe+QsVKlat4u9yo6l3Np7rvP2rU9Kt1n9GuSwD8a99fp26AiwmfivUeeNBuI/UjsH6Dhi4RBDVqXMtpt7Xa0eShpkL0ChDcrHkLu23WgpCWrTTxy0frh+u3sdtyMbRtV10Hv3yEtu9Q8t/Yjp1a6yaooHrnMLs58BDe5RFj/BRUKd/VbiIseHUTc6DqCGjwqN1kaHB2eUwSQYVk9x52EyLQs5dEgnmI6F2y/pN9KkgmmIe+kXbTKoKzX38TGLoxYKDd1DwYNNgcgnlva7+SECad/aIEbB0yuFP00GHDHw8JeSJkxMhRo8eMHTe+coxAwwn2B5DAh1VsjJnYvvMkf2rb2MlTysdFqLSvMtVmhtOCufZNnzE8XqWHhJChT87kdjLLy06GszlTwTlxs0W9ReJTDZI4HAfPtY1g7Dy2WfNr9tHUV3LzOuzBbd+nbWLoP4FlUkCFBTr6S1m4iNVhxGJbGC5hzZlCa+j1gl5LKzH6rPpPGxguY/zkEctTjXQ7lbVYsMJyhs/QGQbMWmmwY+eUVXSOaRYzXJ1ONWPAIAl9x6atKQEc6W9p8BRJ3a9dR+W43kKGSzbQLGjmb7znAvhR4+SzljHcOJ2iPv05qTq6VqT905+3iGECTfsLmyRrcTbOILWEbraGYncKwxkmjCO3VCH1bM20guE2ygs0zBRNWZQY6ZtgPsOnyWnBdj3DNRG0oPwldpjOMP5FQumi1aZpy+5EaMvYYjZFcnKxc5eJ6hIaEPqGpJjLcAqhsdUSUxU6dxMa95iqcCUxblu0zFSF7ue4h+DYxUx9/yIC1TMmM3QPWfdipUGG5jJ8bCGihen/fTf862K1u03T5bUP6+psAUN3fMRjgIwQs1TNxgzHWcLQ4RiFx3IvmaQodTtStD/WIoqO8vjH7WmOnmikZs0Bqxg6svHW7EFT1KzE69aHLGPonoMj5YdN0YJfljhLd1SQH4gzQ8dGb6gkx+yYD5GwH2hvZoaOI+ghLreUocNxdE5x7UtN0BC+EzJclGgxRUfxwapPsgkKhqOHuM1qhsWncRmjzFBwDDJ8LNxyio5d+zzKI8x4TR2ZaNvIz3qGDkeb+/vm+bmXzZmgjoEMN1iwgkLF2pDJZm2ovgIpWrkubREy4Tg4YqPdBsnHs/Ahvmy3PSYALS48brc98uGESzatSkK+j2Sshg/xuM3mxKdmZmUtSU2VOV3tBymesIWY18mprx489dq+00WJMBFnpvvOW352pISkFbjwFmHZZL8AYR3Xv36Ok+LjSnrh+LADhv4+50B/FS2llzgqupJIspzL5X3+yGq9NMOgijes49fn0HiRJMIiBF14U9ckpAns5qJF/Fb2uzRHEz/P/+j1stqHl6NhHxK39NlImPpwVR388nG6XhmN6tJA+w0WEJz7lp7jOsUR10XTcL09aGzO4ldx7GowUycx8Cj6aciffxs07W4ywTJ7xByoOqqlCZ+PhLl37Uwl+Og8WQTzEDRbMIZvBc26mUiwRXSATjLM1/UdIcVwBbWsaQSdw4J1EuHBVyRXB/75TZtJLXtFJwkVVH1XfTQAW5i0t5dwOccchm5Un1QSKGa9JmBq+gsVypfrcvHprKy5qWuzTl5Z/F7Nq+smCgzxMq6pREkLKPp5qxj54p73O6TQ24Y/OGXs4VCV9pf4586h8Aj5BGMv8B/eB9tUM18SRrT7kOuNQ7mZlvDQ10jpDLtW4pjWN/oj0ZV3//p7eMOiHZx+coHkcNkMJ33MtOr6uGnaZoDxn3Dc8tvxzHaVgaDsLI13mJ50/iE9R/0PzGL+r/czj8d8CuT+LZfhigyGPRPK6p3Cb7x8htFnq8mMJp8BsQtSGX7OMOZGRyO9xs4OonebXoveAGYT3pDJMJpuyTnDWVlhb9BPxoYepYpfBkKnTWeYfkjGxtdc+qG161SOU6GQ0fM0RfiCasSXshRM20d9jk9QRDdDmTdlMXyfZsEQiQcVksfSfFn6AVIyFi4UXZNkQCSN4alMeQzd6LiVomNrICl4E0i8Ikd7T0o8jJKeGrmRdjbpErmqA11q63gZuh+tRqr2MWH867xMeVnJ3OuzUKC5BM3JlJoql9rKZ+jGYkr4IHbyD8Dv20vQS2buu74yq2bRanKwU5U4WgJlqhjfmnqPZLjbvFSeJasIbcEpSAZFUcPVBNaSI+Vo0wi6ETif0NcIiaAfvY5BjeE3rGVI5XgLSvjD5KkAbWf2CQwl9H1tLkM3R+Jd7Y9WElAlDWNns5YQLu4r8xMksoiJN3pVP4HfVjNUUeg8VjbYjNxLjG+IgiUw+PmjoYiRU/Zlsaq+KRYwpAwYfeCDQpWXgvQ/Ri9c1WHmN5YwpByFWAi+XoC+fV+3ottY0bcWMXQk4KWr/jCbBZVAyw3UqScMLzkcs4qh28/ho/RXwded0bd6l3BWoH6CM62j6KiP/yPgHHsb9OvP0TcrIB6i2P6fFmzMSmF+9xnSDh/jQvRtXV2eHqUnS39Nk9fnHQXc2puRMdsH7X3MBL9Gai6ybqwOC5z7YB85a+UyXFYwUGt1hS6wHpGAtUkao29jpmk3YTHqQ/KBlk1FU6JQetGW7O+gAYvArlwYLo+ySPtS2QDYQ5DcAy1hxefZjDOOzREJeDD5FvrWVVHremcPlI4htxSR83vQ+RW6ECr5MAB8G05sks3QaARcdDYyRqIBbR58Tpf6Af3dYPhvQqz0aFwxQ1VkFmprrYJIZBsjSd+JBpBoGec4plj1KS1GDIKN12TKZFgLL1rOYwh+C8U+hd+2IMoV5WhJ3WwH20rd49pE1IpqzJAMg9FvDkq7XkBkiEYcVdVeCJSVoaGlKsLIRcsfWbKoYA+eBtQgerou/Bwz4e/zmESGyJnmoRFTeAQUPI++Tv6J6CtKtOpUc9juc8FmIiB3YoM49Zzhzn4SXt0cSNb2irktZsdV2GyyWCsRRBI2zbzCEf8ZyhKZRIspCdvHhcYAMOiuksewFrkD9B5PvieUPUIILCQpug4LbEeggwLy1hVJZ6qyRJgAV6o7kRLNKByDO6hachS2kFX6j+ZMj6ksWsI1QIrfy95L4ZjxhtpaaDnYIEUSQ4oz3a82uodTqgxKfrU/NaPrZ5V+4dBoqySGNGeqmjHXETagLQHemU/jqFKKFlY8M7ovUoBIwgyuM83HfXCwTT38HUjjeJffLyx+ICkJS6sz9QBGxreoMm1ppSe5uznhMClitBSGmp2pB/Bcxi90IX9KGnBtXq9toWwTvg2paXfTc5uOVSlmqcOZ5uNX0GY8QyqRrCbInVlthrKBXBOae8ZQM7kDJz3ONB81QaNHWGIJRInN33i9jgSiMdwfe0qRO+CV79XjTPPxO2jlwxYcg5Zi7nAfDBD9mCe6svhqJ5tjJMFQwJnm4w/QzJsjCZPYqnB79QOyL/JE3wWiLI46namCj0CzHI5kPSDJDxrdgOxEniiqbUfnqNeZKgiBDTmScH+8ArfXW+IUcTV/GkfdzlQjRXjrUm9ur+JPMZxY6CM56nem2igmwJMRv2ugyF3VII+gEhz1O1NtFP+EgtyYgTyqD090HGE/5hhJCAg7UwUdQNtQtmBtqOUOt9fngGwST3QzJfkQcDTiTBXAJfHtbEFYmALHjD9Hd+tSbG8NrSZwdyeJxAPI0ZAzVQCXixuyBesBQRgzQuKUDw8X1mo+CW3iXgpDKd9bjKMxZ6rgftD6P2xBTsx4tWBeEfCA55P7oFH8zUkeR4POVMEv8AdiCzJjRnaxdNOYAi8Er5A4y7eBw9GgM1XwEmjPrm7DjBmZccU/b+r5FM4wZ6kYweQYSXyqzZkqOA06OMKUQzGjUFEZOKd2eY65wmp5qvVWGBwNO9M8dIUdsA/zoZiRUvAxzqf1bBGNBR/mqjoIKsdow840DzCAudh3ucCYkev5dAxRe8CT3DIMfvqjqiFUjgS0OlPy117DXsuvBwR/Uj7z+i9phOdnLgM/FUh9E+Go2Znm4S7o4jW24P+AoLJsfudDihWeBKlwmNazTsAUdY7anakbLeCL9hdbEm6aP+T+5MB3FCuSCg4nAD/r6p8tgaMOZ+pGF9gJ+zaQcDit+8ThGJVLM6PQJT8EPxc6o6jCUbszzQO6woL9IgyEgpMojiYPxwp3KFFOq9iJFi5HfTnqyfAfwxmhoor+c/9LtaLY1qPXdfCNt1ieIIejHmfqIApNcW4EgDtNUTRH46parngL6J9EF8SZHHU5UzfGw2445zFbulSRBFMd0PEy0VOCDI66nKkbA+Fs9DrnZfpblWHDk7BFJioKJnoZB5WjPmfqxgzYz1ccUdU7632Jqz3QHoHwEToaR33O1OGIR+NLzm51uNqt9ZQcB3SiJUD4tnOSo+4DP7Dkm2s75xjfQBcX0NF40Ab9guIpYpijTmfqfojoygxeVsUWLsMkek4VyhOMOekQBeSo15mSB+155XvK8Rg2ZNj+DJL7W9y24hz1OlP3TBFdX8m9X4UXM3yZdwgdRpJ/aOA4q6BRsP57kfCV9NwLnTgxg5NMNQqJfqflAPE7ypGfnAspuhnif9dObtoXM2ZQHU0hbiJpbVe4bF667YSeIjgeJOIb627xpJkxI4mfvNkciWeoFjCTiL+Q8lbcg9+smNFQxUk6L6EGlS2pt6qAODnJX3lgxAxf1cvKOuAmlh1921QNaZ7Prw5HjxkiWZtv40avWsMwFr8/auletJjBdzQF2IXL78XUFmlmGMQY8EmVBpSYkSSYJd4bNwxVX3A0jppYa86DKi16aXY0hWizEzfdYO49lXkYTdireiE3QVHd0RRiAbFFuirQZIY/ENUMJ6rWa66jw9EUgjwKMd9cjheJnZAo9UtqYY6JmKMpRPI5kqOZ7+pwcq9H4NBdWPG0ClFHU4jJRKkE107zbljzI2tu7hWZb14sWogRdjRFeIBQ6jpt0qWKjsbksQvBghDDC6bwf+u5fvUvkmPU7zr6UUU2JcWl9W+CjXvMOONyBUzQdxohllbls6XIPoc2dKVVCNZynVtqH91Gpe6j6L4kvGAliFGnKVpkVbJTRRZNe5LUwVwydWnideuuH6pFq6EcIPFWvit1aQwrWnlxzVEax6B4Sb23+JVaXfaGtTdHnqAV8ZVzEj68G71afiXrZuD5WEB5jjdkdPx8LypBV5z119U1Ie6O52fGi2HkDTpB117L7zd1Y5APNiPdYI/hkY8wCLoqWH5FloK5nyI75hvqrsdbfVkEXe/adVldMlqh/kx/V4l+nHuNop61iaCDKIKj9/7vlG11Ilxs9LWqUBkV0K3qqSiWurhGrwwXD77yqkjrQCA0hnYxYNsxDSq0vD2SchAu++TUtE6VXSpofdneOyPRsdeuhECbqwX/sKjKcXt2j01bsWLoihVfHGzW6O4QoTvT9jXRYZZMbAPmrCG+79NLhAYbVa9ZUfORix3AoOn46xEG7y2qZNJtPFoAL5PFGY5LjV3rc/qsDXd9E4BbjrDerXO5IYIzZ1g96qYDxgyQ+p94ygjBmHHmr7ILAcWM4pUsDDmagHHmLVtqBDtmGHE03jU26TdJNpgxw4Cjafqt8L2EVoARM/Q7mvTuV+zmhECPGXodTdKXP8hfjDUKaszQ52jq1jsh484Y6aDFDO2OJmZ/Az8JF2mbAlrMqE91NKwrRD+O+7rfR3LLO8oFGTOc9MunTiW2vTLl9vKvPzgfdyMP59e1/znt7PNlSjK5fBAxI+x7KsPl9s74jADHjE3U9bOcpXbbaQAoZtRaRGMYbMKtN9YBxoybVEfTy+DtFjZD7YpPxdHYbaQhBKozvIcdjYKOagTvaUejYJsKw3vb0SjYwWd4jzsaBXu4DO9xR5OPmzyG97qjyQcnZtz7jkYBJ2aUAkejgB0zSoOjUcCMGaXC0ShgxYzS4WgU0GNGKXE0+aDGjNLiaPJBixmlxtEooMWM0uNoFDxRqh2Ngsml2tEoiEdro6XL0eQDFpIvXY7Ggw6l2NEU4GDpdTQFcO7wJHblRtptinl4utkZV0Dda3ZuK/0fqj64t7QFVJQAAAAASUVORK5CYII="/>
						</svg>
						<div class="op-div">
						<h5 id="one"><span>1</span> Listen To You</h5>
						</div>
					<p class="is-center paddingcentered">Your treatment plan is about you. It’s centered around your needs. That’s why before we start making any diagnoses or treatment suggestions, we like to simply listen to our clients describe their unique situations, their feelings, and their hopes and goals for treatment. Listening to our clients is the most important part of starting the treatment process off right.</p>
				</div>

				<div class="pure-u-1 pure-u-md-1-3">
						<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="230" height="222" viewBox="0 0 115 111">
							  <metadata><?xpacket begin="﻿" id="W5M0MpCehiHzreSzNTczkc9d"?>
							<x:xmpmeta xmlns:x="adobe:ns:meta/" x:xmptk="Adobe XMP Core 5.6-c138 79.159824, 2016/09/14-01:09:01        ">
							   <rdf:RDF xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
							      <rdf:Description rdf:about=""/>
							   </rdf:RDF>
							</x:xmpmeta>                          
							<?xpacket end="w"?></metadata>
							<image id="Plans" width="115" height="111" xlink:href="data:img/png;base64,iVBORw0KGgoAAAANSUhEUgAAAOYAAADeCAMAAAAenYxJAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAC9FBMVEVEmLREmLREmLREmLREmLREmLREmLREmLREmLREmLREmLREmLREmLREmLREmLREmLREmLREmLREmLREmLREmLREmLREmLREmLREmLREmLREmLREmLREmLREmLREmLREmLREmLREmLREmLREmLREmLREmLREmLREmLREmLREmLREmLREmLREmLREmLREmLREmLREmLREmLREmLREmLREmLREmLREmLREmLREmLREmLREmLREmLREmLREmLREmLREmLREmLREmLREmLREmLREmLREmLREmLREmLREmLREmLREmLREmLREmLREmLREmLREmLREmLREmLREmLREmLREmLREmLREmLREmLREmLREmLREmLREmLREmLREmLREmLREmLREmLREmLREmLREmLREmLREmLREmLREmLREmLREmLREmLREmLREmLREmLREmLREmLREmLREmLREmLREmLREmLREmLREmLREmLREmLREmLREmLREmLREmLREmLREmLREmLREmLREmLREmLREmLREmLREmLREmLREmLREmLREmLREmLREmLREmLREmLREmLREmLREmLREmLREmLREmLREmLREmLREmLREmLREmLREmLREmLREmLREmLREmLREmLREmLREmLREmLREmLREmLREmLREmLREmLREmLREmLREmLREmLREmLREmLREmLREmLREmLREmLREmLREmLREmLREmLREmLREmLREmLREmLREmLREmLREmLREmLREmLREmLREmLREmLREmLREmLREmLREmLREmLREmLREmLREmLREmLREmLREmLREmLREmLREmLREmLREmLREmLREmLREmLREmLREmLREmLREmLREmLREmLREmLREmLREmLREmLREmLREmLREmLREmLREmLREmLREmLREmLREmLREmLREmLREmLREmLREmLREmLREmLREmLREmLREmLREmLREmLREmLREmLREmLREmLREmLREmLREmLREmLQAAACPWwy7AAAA+nRSTlMABjxskrrb7/DcPSF3u/a8Igxmw8QNFIgKgPRM4uNNoaMOLt1Q9VJp/Wt4aFFTLdisg3Bg/mEYSvqYIwnhuCS3ffdk5DCGHx6H5SD4MmK/wAG+/Copc7SnBPEsNsvKN3FviSuKsO3qsc/C0Zyai36B7rPfqUVEk5DrBelY1tJfVggTEq/V13t6PhXFhR35ds1Zbpl/Z2UPuQID0xzURkfgC67oGoLyG1p8O1wvntCgzMYZ5hHI2b2bjZ9qbT/7XUBBW07BqCiEQhYQeYxLj5RJMcd02ge1oler7K2dqt5IOThVyZWOkTWk5840crZDT7LzMyYllpcXpTp17Mzk4wAAAAFiS0dE+6JqNtwAAAAJcEhZcwAACxIAAAsSAdLdfvwAAAAHdElNRQfhAgMQAxfNYet9AAAQrklEQVR42u1de1xU1RYexceQSGi+mGupqFc0UwNEAvMaaAaUjKgEhmmKZkaKWQo+rmQ+IjXzgQ5pamppapbPRKXIfFteNfOReSmuN5U0S7tZ5687KK+19j7nrH1mzmz4/fj+Y2Z/a6+POWefffZeey2LxWOoUdOrVu06dRUnrN61a3ndU8NzfXsK9Xzq+yoIvvf6+Mn2y51o0PA+RQWNGjaW7Z2b0KSpVdGAtWkT2R66Ac38bYoObP7NZHvpKv7WXE9kMe5/QLafLqFxC4rIYrRsJdtX4wjwpqpUlNYBsr01ijZt6Sqdj5e/y/bXGNoFiqhUlMD2sj02gg5iIovRQbbP4nhQXKWiVLnfs6PgFVty3T4k228xdBIafSqMQ1VqvG2s9iTp3OXhoODgoIe7dFZp0LoqPT+5s4KQ+l1Dy5uEBtcP4bVqKdt3Orpx3A97JBw383skjNOwysz7Irqzzj/ag9cy/B9sy+ZVZR7fk3H9sUi1tpGPMY39ZftPQxMbdjyql3rrgCjc2lY13j97Y7+9H9dq3ocZlZ+QrYCCBnWR19Ex2oTH8e9pbSBbAwHByOnYJ/UYT8YiylOyNRDQF/n8oD4Fz3/jZGvQx+PooR9n1+fY+yGdMfocyQiCDsf3p5AGxEPWQNkqdJEAHX6axmoJWQmyVeghMQk63IlGG4QenYmydejgGehvMpU3GPKela1DB12hu0OovKFV65HyHHR3EJU3DPKGy9ahgxTg7Qg7mTgSEJ+XrUMHo4C3L9CJcYA4WrYOHcD56Yt0YiogRsvWoQO4x/cSndgUEK2ydQjJFHhDHgOIj8nWISRzLJ3oX6V+zTTg7Tg6EQ7RdWXr0AHctY2iE1+uUkMQXPEIoxPh0ld32Tp0gOam5MXIZvA1tbI/N8dDma9Qea9CnsBNLQVovWMClTcR8shTfknoA91Nz6DRMiZBXqVfJbkf+juZxpoMWQIjtCTAuakyxU4h2adClsBcWBIegg4rXSmkfyLSNNkqdJGJAvTSX9PnTIcvm0pShGwV+kBXrTLOrsewv44oleaajRk+Iz5w5kTepmVN5LMyS8/YbMx4g9Moa/abI9KS58z1pEqvkhDgeW9xvpyPvda5PfGmizKK0+jtkncC3wUeE5m4sNylRezXk7HbgYu1rGUzoTWch9BTFa5o4qPYVSxZWlECu6LqGIX9VoarLi/nPME0fsfBtOpRMYC8/hJPqFy2HI4wbIt3Gc+VRuF8Y+Gc6PAVbLOFoMGUZear7LESXZGr2Dbvsb7H+nP2ZhssYiMPlBROpyNgkyhuvIY70Wk1douzS7KGF9xufRhNVGPW1uU0e/8D1t4HuFE6cWvGKJ5NY/zihU+sU7i4b33Nkvuq1YfrVQ4zbOCYi2FaJZm6ybIxifUrlNdwk6KGSR81avTRJNWvN/OsRcQz7czU2Smd9Wsmt2XmYMUQPuZP8z5hW5p33Q7gqFQLLQhdqSuJg+5b+NZ4N0H6MHNUruHEqCmNclRa99gqrnK12ghqr81pfb8pz5UlH/EusizV9v2j9FRhRKlvE27bzmk/1YR5Qo36nI5StFbu/HaIqdwRrmGsGedZrOx0/5nBTznd7MrRpEzfKaKyn/aLqeMlDifX3Sp3czrx0iPl9A5RiAhpmqNnzYtD2+NelXvzmB4CPyPwPn9ZX2ExXs4nGPuCfXzGU3hkLBvJdBDWhsRs9tw8fZFhw2mr8/vYaOp0N75n53zJeraPSg5/WufKDdlPnolzdDZyXwjRWPaKFTnQdSDVV12k76cHBEztY6/bg+5SuZc9OvOFmIXGwTu5P2lI32DBM8dfMHYCD7lHZSv2pKmB0MGsyY/UgUbqLJx8WNxOEONM9BG3yGSfmBMNWorYOO3osdzNm3OPHZ220egBhV2MO5sNWgJglzxSHK5bNQ47Ox/6ynWr7CW7XHIo+qqp2KNo1ye3X2ObaeEu23QRfswSxnFXTW5kRvB/yVbpvI/wcBt4wjWD9kZYZaWIjWyKvTppd8neBmxvu4fWvLWRcRL7RdpfVEMEHn9sfrIV3kXM+8ixKFf2Ck/hf1qlOfycjT1z4fj5Ebxe3NcuW14pmNUhK2G7WAX4TvetJ1tdOfrg09sCQa0Qr9mQJd0NWU+iAx42jJ6H9EeGvjktW1pF1MCn0OcYs9MKb/i8K1sZxCv47jT2pvIWMvOobF0Y37rjnkpEz8yQjbJlYQSgOV+0kakLjmM6I1sVC5xe4awBG2iZK970HWJxxKD3ivvETQSg/5RbXtHdjc3IyV7CFlCAawjpuKmncQDdnbVEDWSgvcxzshXxcQ56eV5072gfuhzukS2Ij3uQm6L5wL6D9FGCdI8BhVqliLEboIAdl95azQQ6FjtPLN3QNMh+v9JmfWmG3q+JQecl2A/JF2SrUceL0NPvRbinbZD8hgjZs0DhuzaRxRI0+b9oly1GHfbW0Nd/C3DRPv8u2VpM8nU0pBbIlqKFH6CvU+nMw5C5VerWkB4cKM4qlMw8C4mfylaiDXRU4m0yEU3bf5QtRBs/Qm/p0/dCwAsxvgLqEUyHMQODqbxMGK/xsWwdekgG7uZRn5wF8CowugXvMaB77EMirSGkGVlh8SjQiNmOSKsFaX1ky9ADOhr7HyINZkGZJFuFPuBpii+JrPOAVV+2CH3AUN80GikHXgNfyxahj0vQ40wSKROSKnu+KQs4KFcMYkAPPHRAzlMlDxuBw6uJrP9WJHW3yxahD0d0RY83EVnhFde7dsvWQMHACg7Hk5OJtysnPWqXLYGCxArhSz3pNK/SWe25SrukB7GkLFlobxHagE3pijKvfpXJym1xdB3t1BibQJ3QlsIe+kEVK4CV9VNApmwfqlGNalSjGtWoRjWqUY1qVKMa1SiFI2DD2u+TL1qt1jTvwstjpokcgvYADkwbc7nQO83p3sXk79duCDAUOrCld6FNQYi+3NU9B3xdxpGul6Oxd7bCOfS4gzvIXMccBS3BvHP7pBfxSNx3Ti2Zwui3M+n/qjEjFQ2sHCo1RWXEUM1URCPH0C640z6a1bSLcSVbWgiNI/uKnndWH8KxoFdb65kpxtUiOSqLrlK8a/2qjpkGqRQzTsQflHBeNeNgPNG9VM1dsf6diWacOOmB3IUQy07SveuscQThARvdjqJM+sGzKgsmiXhnU90xWCxYtteXnGXGHdjnK+adWh7VU2JmnAhb5zmV68KE3TvFs9NO2IxI1iCXf0txldwIIWOFpn09FGpbIHjF3kVgR2wnwJAd5zjkkfF22SRj3uEy2K3UJgW2ncf9fz7lnxp3XqWBJ/I9ZGxX6fx8XKr/qZ/9j++0qTRAZbBbchslXbte1sLRy2c5t5HQ9rAx9OZ2vNynV/mc89C3SdxGLSva+YXTIL4lk0uz4AavnenzviLe3OcGMyoMa8lr90t5g1WcdJV9udOIAjb9tXLV5Hm8gzOPHcUd+vr3ZVtGrSr7+iDzZWx7O7/PxLHsiKxalNo9iGQ6DByr8s5rbx/LNC7L3LYFJ4RQtmqU7V3B3ARXTH3/jGDevJJWqLd+kskX27Y05y2TBMxbMynHCSYbr6mZ8xfg3tJPaDWvx5TBLjmAc+RX9Lleoenf8O+50sR1k0S8VpD0mzYhBi8S/Xp3OWE3vi91C01/hf9h3cyT2Q11FaKbho0pg303Hq8QfUooNI3HrPHmyUSlrCi5IHEZ7MLiD2+iDynR0BlT0AVgWhmEJeinmUKZdOHEyDedn62HH4XdpPS+Fxmi5Ko1hM9QR3sppJvodWa9BQfwU/MUo7rhpiV/QAkc+tFYuZAV53wsoaviFs0QquLW3CyZKP+L3pJdCW6heyrCcgh+Qj3dYEfjvJ85Kv1gLyvtRB5KqHIIF4xeTLRjmQN5v1B5YkDvFOQET4shb6jld/gBuerc55D3FpUnBpSa6XMqLwbyfkdjCf0eOw13a6hx9IKAlTjm0ZOJwXu6nwUuGyTQPXgHGTIF8Ed4h05MAMTWFjihvUQ39B0gLjdHJlyw+I5OhAeNfrXAi1gg6xfcbVlpjkw4nqfSibOgLiQziG4IHg41qZQkXNYQODAbZI5Mj/yabpO5iG5ok+dlCgzni5BMuP76O93QNUCcb45MWC/wGp34P0A8b4G1WQSeCxcBcak5MpeCTi7SifBJtMMyDvw9grwYuQ2u8JFLGothAugkcBuV54B15Mbh9S5ypro2kOfm+iul2AN7oRUncaIX5O3Cq6CzqYZQkAK5DrcYUIkA8oMTVS6NtBTBD3YQ7eASuFlEniCyYC/WTCIPVYMqsjhQyvt8mh2UEc20GvCjYT/EDHH5kJXmHHFS4Ee08/8OFHJiWkIPlIijM22IRGtexdn38GY86S7D+cbdUMOCD7wknE0h4TzExZvzWaioVB3CS10TVPeTWjpeHDgX5whCIuzTqL5M3p2B43mkXb8chX0coghnEqUDJUghlMG2HEeU5+98itMKK7phMEyhaRN3cotwX0f1GEx5x4fufIyTRCthOrfnc9jOJ+ap5NTeXKvd/hUcWtO8ZCOLqXLkq7n1w1ZKJU9OjKAN091CrebdmJCY0pfLCCaeOk89qUPOi0y3V+1myrSze/Kp6vUAdzNV+qLLNpmZcueKsn8638xcThXGfDNVMs/6YuxUKXk3fT/bNrjsWwcn7uZ8Q87ebIRPW7al6fmWz7F9tvXhBAIkNuQEL22vMKEYxBZkVJTW7dF23h8deAHYVsHjAuII5UWlX+nwB2y1pD0vhCsPJMjpoPAQ2yX7Vsk/I3HQnmt53EZiiW8NYTK347xrewaVXHGOW9ldYrmNYDUYR21FBbFT41JuxM3MU/veIxl6L6j1HjYz7kZK3NRYte9roznwa4YKMCtKcqYnZGYmG/NuJZPwcUBdI3bSTb8x7yI03Yh3dQewlp41EGoaRtz0dR0388S98+UWZM9PErUT76bSlxSsoJ7NKENSPt/SMyPF7Ng8eubmJ5uYdyOfUbNUb7mInfkmrf+oIWu+iHfLNQLyGtym27nk8WNFGZfo3t3WTra3IY1m5n71KMhVf7hWoeqI+v9vBbFOfdoGvU5CKT9o/AS12gbT59Rxzk9eGLLGmMQ1Q+LClPipR9V+i1YTKCPRbcpj7s84PTMpw9S410sXiQLvjRR+pIZGltVD3qp68GNYip53cX8S+9uboFFRO+/2CVXihxXf3EMKZxXpFoovRU7RrMEVO/VV3+U4cVvjGRqSQAp3K8Gyo1P4Zgbv0Rhfa3THzZMS/qq5Sq+zVTX/SmCe2Ts0YnSz9gzmezflqPCxmGELUtBwtHp/tp8m5Slu34EzzszuOCCTR8gc0HHWmRn880zabz5+2fth8l0lLWXBMIsh2Ode9zrWIsWJC8Mb7t2i2/51rVsmfX5C7qWeQbuDnWgf1PNSbsIozYmqft2yLXsbDr9Q7F2LY17X59qNaTSAGYobUegxt0XxjTtlmrTN7wYsdV1cOd6TrUYVQ9wpkxwL6nFsG+G6ulKsllzSXQtnXZdXghCTAnTdg2wjZ4M5mFdpC5fdRcB7BlYzMAJfJ+d/lobD7d90TeTJPYdla6AhZkhf4ZWbu4iPG+gn23sRbFvXgvgKXI6oy+vIYVyVCD0iz3QOoSkM6XwmshJWpSWjcf6p3JOaCYisJ3NP5TeW7adbEFpwduDE8bWTvdOsd37eQOsI7+Ta478eeLbAM8v2/wdFKtW5HvSXhwAAAABJRU5ErkJggg=="/>
						</svg>
						<div class="op-div">
						<h5 id="two"><span id="two">2</span> Look At Your Options</h5>
						</div>
						<p class="is-center paddingcentered">Once we have gathered enough personal and clinical information, we begin to craft the best treatment options for our clients. We look at all factors - specific therapies, session scheduling, health insurance coverage, etc. - to ensure we’re giving our clients the most personalized experience possible. We want to set up our clients for success during treatment, so they can have success after treatment.</p>
				</div>

				<div class="pure-u-1 pure-u-md-1-3">
					<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="220" height="220" viewBox="0 0 110 110">
							  <metadata><?xpacket begin="﻿" id="W5M0MpCehiHzreSzNTczkc9d"?>
							<x:xmpmeta xmlns:x="adobe:ns:meta/" x:xmptk="Adobe XMP Core 5.6-c138 79.159824, 2016/09/14-01:09:01        ">
							   <rdf:RDF xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
							      <rdf:Description rdf:about=""/>
							   </rdf:RDF>
							</x:xmpmeta>                      
							<?xpacket end="w"?></metadata>
							<image id="Calendar" width="110" height="110" xlink:href="data:img/png;base64,iVBORw0KGgoAAAANSUhEUgAAANwAAADcCAMAAAAshD+zAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAABuVBMVEVoj0Noj0Noj0Noj0Noj0Noj0Noj0Noj0Noj0Noj0Noj0Noj0Noj0Noj0Noj0Noj0Noj0Noj0Noj0Noj0Noj0Noj0Noj0Noj0Noj0Noj0Noj0Noj0Noj0Noj0Noj0Noj0Noj0Noj0Noj0Noj0Noj0Noj0Noj0Noj0Noj0Noj0Noj0Noj0Noj0Noj0Noj0Noj0Noj0Noj0Noj0Noj0Noj0Noj0Noj0Noj0Noj0Noj0Noj0Noj0Noj0Noj0Noj0Noj0Noj0Noj0Noj0Noj0Noj0Noj0Noj0Noj0Noj0Noj0Noj0Noj0Noj0Noj0Noj0Noj0Noj0Noj0Noj0Noj0Noj0Noj0Noj0Noj0Noj0Noj0Noj0Noj0Noj0Noj0Noj0Noj0Noj0Noj0Noj0Noj0Noj0Noj0Noj0Noj0Noj0Noj0Noj0Noj0Noj0Noj0Noj0Noj0Noj0Noj0Noj0Noj0Noj0Noj0Noj0Noj0Noj0Noj0Noj0Noj0Noj0Noj0Noj0Noj0Noj0Noj0Noj0Noj0Noj0Noj0Noj0Noj0Noj0Noj0Noj0Noj0Noj0Noj0Noj0Noj0Noj0Noj0MAAACFAEriAAAAkXRSTlMACXHI9v7lojM0ci/fjAMk7pLBQULCOLh/+QafHB+csCAwoA0uQMRYZARLnd7mRoaqAaR4/MxQI26s8qOQ3Z6l1s8Y9SjY0MCnJ4kKTGNI/WD6xhSx++JzsjY/JfMCU+DHhPhsHuR8D9Uxvvf0qx3Te7yhgmJHEVXoFmmTBfE3dCpf2XUT40rN614SMqgZ7BptLn5E6QAAAAFiS0dEkpYE7yAAAAAJcEhZcwAACxIAAAsSAdLdfvwAAAAHdElNRQfhAgMQAzkRt+ayAAAFkklEQVR42u3d+VsTVxQG4AuyuuBoVUyiFgpGokGJiiK4K9pqCqJi3avVFlHBDbXWvQqI4jL/cYFEMndmkswM98y5Z3q+30KG85z3AfJNlmcQwksqKhdVVdfU1nk6GDT1tTXVVYsWV6ibuGSpmcuyBlxaw7L8IkuXqBq53DC/ZwUubsX8IsZyRSNXmoX8gGlbZVlktZqRaywjzUZMXKN1kzVKRq61jozF8WzxmHWTtUpmJqwjzXV4uPXSIhuUzPxRmtmEh2uWFvmJcYxjHOPwcC2tGx1JSjM3bURLm7RIo/OA1paisNTmLWmTeNJbNqdcaBWV7dibqUl7peP50FbyP7RC0lslWsM27IXUZpvleWe8A3sb1emYP7/PbMfeRX22Z/K4HdibQGRHzla/E3sRiOysn/ul7MTeAyads7+Yu7C3gMquGVxy4WP0TFKI3dg7wGW36MJeAS5dYo/8he6evfbskw7YvxctB6RFDjru7+mWLXvEQen2ocPOU2o6z+cOH5L54oh0+6jLTDo4cVQ64oiQbva6zSSEE73SITLuGHXcccb9X3A/64L7BQB3QjrkJB7upLTICSW4rPWITjybENLzl6wSXNx6ivIrJs76wmXa9Y1C3zjR1z9/wMqUQEyq8AZ2f5/rEf5x4tRA/v7TZzBtQpw5nV9k4JT7AQFwovXs4My95xYjvmecS/y3czOLDJ5vLXJ/EJwQLRcuXsKW5XLp4oXibwkEwxEJ46iGcVTDOKphHNUwjmoYRzWlcBEL46iGcVTDOKphHNUwjmoYRzUyruoY8VwugYv0Ux7G6RzGUQ3jqIZxVMM4qmEc1TCOahhHNYyjGsZRDeOohnFUwziqYRzVMI5qGEc1jEPNlasdvb9fux7kMqHa45qu5Zb540b0cDf/nF/nr6jhhm4V1ukfjhYuftu6T0ekcJkeaZ87V6KEu27K8fuIqTOuzWYz10cH12e/dtxdv1fZ1xc3fNlmM0f8jtAWN+q4NGruenlRwA3ds9vM+76HaIqLP3DYHmZ8T9ETl3nosD0IcCUIPXGPHLZ7QwHGaIkbc9hqRoPM0RH3eNBu624ONEhDXHO33Tb4ONgk/XCjNY5fyrGAo7TDuRTco6CzdMOpKTg9cYoKTk+cooLTEqeq4HTEKSs4DXHqCk4/nMKCA8Y9yY49bfL1HSoLDhS3rnbu9Y/VPv5xmtKCg8TV/Z2fMNDm9VvUFhwg7pnl5Y9/PH6P2oIDxD23DvGmU1xwcLgXL02/OtUFB4cbtu1ZXqe84OBwq0yfOvUFB4cbNv3pAAoODmf7myungyg4OJz8aFlOB1JwgLhn7T50IAUHiBP1Lz3rYAoOEideedUBFRwozqsOquBgcd50YAUHjPOigys4aFx5HWDBgePK6SALDh5XRgdZcCHgSupACy4MXAkdbMGFgiuqAy64cHBFdNAFFxLOVfcauuDCwrnqHFFccKHhvOhUF1x4uPI65QUXIq6cTn3BhYkrrQMouFBxpXQQBRcuroQOouBCxhXVgRRc2LgiOpiCCx3nqgMquPBxLjqogkPAOXRgBYeBs+ngCg4FJ+kACw4HZ9UBFhwSTrzJ/8NhYxO4DeETRKm3Z/9N3373Ht6m08ejGMc4xmEvxzjGMY5WGEc1jKMaxlEN46iGcVTDOKphHNUwjmoYRzWMoxrGUU2yBG4ce7mFZlzGGdLNCeztFpYJCWOISen2B9K6iQ8SZlIkTDnjyeNEkxy3URJiyoxspsTHGPYOUIm9F2IEewmozF758xP2ElCpm32MmcbeAibTcw+gn79g7wER42u+HvqxN1Gf6uz38vuGvYr6dBWq/VvEfnbVXdKJS6T+7oysfFr2OUKPmdNfHWedn0Yica4SG6lzPan+OJWYNBY+Hi/GZGLK+qnO/wDoCml+JK0ujgAAAABJRU5ErkJggg=="/>
						</svg>
					</a>
						<div class="op-div">
						<h5 id="three"><span id="three">3</span> Schedule Your Visit</h5>
						</div>
						<p class="is-center paddingcentered">Our location is ideal for residents in the greater
						<?php if( get_field('staff_gallery') == 'chicago' ): ?>Chicago
						<?php elseif ( get_field('staff_gallery') == 'detroit' ): ?>Detroit
						<?php elseif ( get_field('staff_gallery') == 'san-diego' ): ?>San Diego
						<?php elseif ( get_field('staff_gallery') == 'san-francisco' ): ?>San Francisco
						<?php elseif ( get_field('staff_gallery') == 'la-encino' ): ?>Los Angeles
						<?php elseif ( get_field('staff_gallery') == 'the-canyon' ): ?>Los Angeles
						<?php elseif ( get_field('staff_gallery') == 'atl-roswell' ): ?>Atlanta
						<?php elseif ( get_field('staff_gallery') == 'atl-midtown' ): ?>Atlanta
						<?php elseif ( get_field('staff_gallery') == 'nashville' ): ?>Nashville
						<?php elseif ( get_field('staff_gallery') == 'memphis' ): ?>Memphis
						<?php elseif ( get_field('staff_gallery') == 'michaels-house' ): ?>Palm Springs
						<?php elseif ( get_field('staff_gallery') == 'talbott-columbus' ): ?>Atlanta
						<?php elseif ( get_field('staff_gallery') == 'talbott-dunwoody' ): ?>Atlanta
						<?php endif; ?> area. We make it easy to schedule a visit to tour our space, meet our friendly and experienced staff, and speak face-to-face about your upcoming treatment experience. We pride ourselves in being flexible and making things convenient for your lifestyle and commitments.</p>
				</div>
			</div>
	</section>

	</section>


	<section class="big-stats">

		<div class="gold-standard pure-g">
			<div class="pure-u-1">
				<h3 class="is-center">Foundations Recovery Network's</h3>
				<h2 class="is-center">Gold Standard</h2>
				<img src="/wp-content/themes/Rehab-And-Treatment/mobile-images/frn-gold-standard.svg" alt="Foundations Recovery Network's Gold Standard"><p class="paddingcentered">Through our integrated treatment approach, our programs annually meet criteria for “Dual Diagnosis Enhanced” (DDE) services, a standard only achieved by the top 5% of addiction treatment programs. Eligibility for DDE services is determined by the Dual Diagnosis Capability in Addiction Treatment (DDCAT) index assessment. In 2007, Dr. Mark McGovern, a member of Dartmouth Psychiatric Research Center and one of the developers of the DDCAT, classified FRN’s program as the “gold standard” in the treatment industry.</p>
			</div>
		</div>

		<div class="pure-g is-center">
			<div class="pure-u-1 pure-u-md-1-3">
				<h3><span class="big-digits">80%</span><br><span class="bd-subtitle"> RECOVERY RATE</span></h3> <p>vs. 30% national avg. of abstinence after 1 year</p>
			</div>
			<div class="pure-u-1 pure-u-md-1-3">
				<h3><span class="big-digits">10,000</span><br><span class="bd-subtitle"> CLIENTS TREATED</span></h3> <p>Over 10,000 recovery stories, more each day</p>
			</div>
			<div class="pure-u-1 pure-u-md-1-3">
				<h3><span class="big-digits">1,250</span><br><span class="bd-subtitle"> CONNECTED TREATMENT CENTERS</span></h3> <p>Hundreds of locations for <?php if( get_field('staff_gallery') == 'chicago' ): ?>Chicago
						<?php elseif ( get_field('staff_gallery') == 'detroit' ): ?>Detroit
						<?php elseif ( get_field('staff_gallery') == 'san-diego' ): ?>San Diego
						<?php elseif ( get_field('staff_gallery') == 'san-francisco' ): ?>San Francisco
						<?php elseif ( get_field('staff_gallery') == 'la-encino' ): ?>Los Angeles
						<?php elseif ( get_field('staff_gallery') == 'the-canyon' ): ?>Los Angeles
						<?php elseif ( get_field('staff_gallery') == 'atl-roswell' ): ?>Atlanta
						<?php elseif ( get_field('staff_gallery') == 'atl-midtown' ): ?>Atlanta
						<?php elseif ( get_field('staff_gallery') == 'nashville' ): ?>Nashville
						<?php elseif ( get_field('staff_gallery') == 'memphis' ): ?>Memphis
						<?php elseif ( get_field('staff_gallery') == 'michaels-house' ): ?>Palm Springs
						<?php elseif ( get_field('staff_gallery') == 'talbott-columbus' ): ?>Atlanta
						<?php elseif ( get_field('staff_gallery') == 'talbott-dunwoody' ): ?>Atlanta
						<?php endif; ?> residents</p>
			</div>
		</div>
	</section>

	<div id="modal">
	  <div class="modal-content">
	    <div class="header">
	      <h2>Addictive Behavior</h2>
	    </div>
	    <div class="copy">
	      <p>When substance abuse becomes a part of a person’s life, addictive behavior can become destructive to that person and their loved ones. Our integrated treatment model compassionately treats the addiction to and abuse of alcohol and drugs.</p>
	      <a href="#integrated-treatment">Close</a> </div>
	  </div>
	  <div class="overlay"></div>
</div>

<div id="modal2">
	  <div class="modal2-content">
	    <div class="header2">
	      <h2>Mental Health Issue</h2>
	    </div>
	    <div class="copy">
	      <p>Many people who struggle with addiction have underlying mental health issues. One cannot expect addictive behavior to stop without addressing these issues, like depression, anxiety, bipolar disorder, post-traumatic stress disorder, and many others.</p>
	      <a href="#integrated-treatment">Close</a> </div>
	  </div>
	  <div class="overlay"></div>
</div>

	<section class="integrated-treatment" id="integrated-treatment">
		<a name="modal2"></a><a name="modal"></a>
		<div class="is-center">
			<h2>Why Does Integrated Treatment Work?</h2>
			<p>We treat mental health issues and addictive behavior AT THE SAME TIME.</p>
		</div>

		<div class="pure-g">
				<div class="pure-u-1 pure-u-lg-1-3 mobile-hide">
					<div class="pure-u-1">
						<h3>Addictive Behavior</h3>
						<p class="is-center paddingcentered">When substance abuse becomes a part of a person’s life, addictive behavior can become destructive to that person and their loved ones. Our integrated treatment model compassionately treats the addiction to and abuse of alcohol and drugs.</p>
					</div>
				</div>

				<div class="pure-u-1 pure-u-lg-1-3">
						<div class="circles-wrap pure-u-1-2">
							<a href="#modal" class="addiction-circle">
								<p>Addiction</p><p class="desktop-hide">Learn More +</p>
							</a>
							
							<a href="#modal2" class="mental-circle" ><p>Mental Health</p><p class="desktop-hide">Learn More +</p></a>
							<h3 id="circleh3">Integrated<br>Treatment</h3>
						</div>
				</div>
 
				<div class="pure-u-1 pure-u-lg-1-3 mobile-hide">
					<div class="pure-u-1">
						<h3>Mental Health Issue</h3>
					</div>
						<p class="is-center paddingcentered">Many people who struggle with addiction have underlying mental health issues. One cannot expect addictive behavior to stop without addressing these issues, like depression, anxiety, bipolar disorder, post-traumatic stress disorder, and many others.</p>
				</div>
		</div>

		<h3 class="is-center">Integrated Treatment focuses on treating the whole person,<br> not just one condition.</h3>

	</section>

	<section class="program-details" id="program-details"> 
		<h2 class="is-center">Program Details</h2>
		<p class="is-center">We give our clients a vast array of treatment options.</p>
		<div class="accordian-wrapper">
			<div class="accordian">		
			<ul>
				<li>
				    <input type="checkbox">
				    <i></i>
				    <h3><?php echo get_field('first_detail_title'); ?></h3>
				    		<?php 
	    
								if( have_rows('first_details_description') ):

								    while( have_rows('first_details_description') ) : the_row(); ?>
								        
								       <p>&bull; <?php the_sub_field('first_description_bullet'); ?></p>

								    <?php endwhile; ?>

								<?php endif; ?>
				    			
				</li>

			 <?php 

				// check for rows (parent repeater)
				if( have_rows('program_details') ): ?>
					<?php 

					// loop through rows (parent repeater)
					while( have_rows('program_details') ): the_row(); ?>
						<li>
			    		<input type="checkbox" checked>
			    		<i></i>
							<h3><?php the_sub_field('details_title'); ?></h3>
							<?php 

							// check for rows (sub repeater)
							if( have_rows('details_description') ): ?>
								
								<?php 

								// loop through rows (sub repeater)
								while( have_rows('details_description') ): the_row();

									// display each description_list_item
									?>
									<p>&bull; <?php the_sub_field('description_list_item'); ?></p>
								<?php endwhile; ?>
								
							<?php endif; //if( get_sub_field('description_list_item') ): ?>
						

					<?php endwhile; // while( has_sub_field('program_details') ): ?>
					</li>
				<?php endif; // if( get_field('program_details') ): ?>

				</ul>
			</div>
		</div>

	</section>

	<section class="testimonials">
		<h2 class="is-center">What Our Clients Say</h2>

		<div id="slider1" class="csslider infinity">
			  <input type="radio" name="slides" checked="checked" id="slides_1"/>
			  <input type="radio" name="slides" id="slides_2"/>
			  <input type="radio" name="slides" id="slides_3"/>
			  <input type="radio" name="slides" id="slides_4"/>
			  <input type="radio" name="slides" id="slides_5"/>
			  <input type="radio" name="slides" id="slides_6"/>
			  <input type="radio" name="slides" id="slides_7"/>
			  <input type="radio" name="slides" id="slides_8"/>
			  <ul>
			    <li class="slide"><img src="/wp-content/themes/Rehab-And-Treatment/mobile-images/testimonials/RichardS.jpg"><blockquote class="testimonial-quote">My favorite part of the program was the individualized treatment and the personal relationships that I had with the therapists, counselors and staff. At any given moment if I needed to sit down with anyone, they were there to talk. It didn’t matter what time of day it was.<br><cite>Richard S.</cite></blockquote></li>
			    <li class="slide"><img src="/wp-content/themes/Rehab-And-Treatment/mobile-images/testimonials/VanessaW.jpg"><blockquote class="testimonial-quote">The person I was when I entered treatment was very dark. I didn’t think that they would get me, that they would understand what it was that I was going through. So, from the beginning I had a rough time opening up to them. By the end of my stay, they were friends.<br><cite>Vanessa W.</cite></blockquote></li>
			    <li class="slide"><img src="/wp-content/themes/Rehab-And-Treatment/mobile-images/testimonials/BrentW.jpg"><blockquote class="testimonial-quote">My life was centered around my addiction. If I hadn't gotten the help that I needed at Foundations, I would have been dead or held up in my parents' home living a miserable existence.<br><cite>Brent W.</cite></blockquote></li>
			    <li class="slide"><img src="/wp-content/themes/Rehab-And-Treatment/mobile-images/testimonials/MarkS.jpg"><blockquote class="testimonial-quote">My parents feared for my life. Things were getting out of control. If someone would have told me that it would have been as easy as picking up the phone for Foundations, I would definitely pick up the phone and call. It's the best thing I could have ever done.<br><cite>Mark S.</cite></blockquote></li>
			    <li class="slide"><img src="/wp-content/themes/Rehab-And-Treatment/mobile-images/testimonials/CristinaL.jpg"><blockquote class="testimonial-quote">The treatment plan was customized for my needs. As my needs changed, the program changed for what I needed it to be. This program is about giving yourself the chance and the opportunity to become all those things that you don’t think you could ever become. Just give yourself the chance.<br><cite>Christina L.</cite></blockquote></li>
			    <li class="slide"><img src="/wp-content/themes/Rehab-And-Treatment/mobile-images/testimonials/JanetM.jpg"><blockquote class="testimonial-quote">This is a place that’s so nurturing, that’s going to help you get through the problems you have to get through. They’re not just telling you how to get better. They’re showing you and giving you life lessons. They gave me my spirit back.<br><cite>Janet M.</cite></blockquote></li>
			   <li class="slide"><img src="/wp-content/themes/Rehab-And-Treatment/mobile-images/testimonials/JenH.jpg"><blockquote class="testimonial-quote">Foundations isn’t just a detox center. They take special care to try to understand what those things are that make a person use in the first place and address them, giving you very personalized care. At the most desperate point in your life, Foundations will be there. There is hope.<br><cite>Jen H.</cite></blockquote></li>
			    <li class="slide"><img src="/wp-content/themes/Rehab-And-Treatment/mobile-images/testimonials/MarthaF.jpg"><blockquote class="testimonial-quote">Before my son went to Foundations, there were people who said to me that he was hopeless. But that just wasn’t true. Nothing else worked until we found Foundations.<br><cite>Martha F.</cite></blockquote></li>
			  </ul>
			  <div class="arrows">
			    <label for="slides_1"></label>
			    <label for="slides_2"></label>
			    <label for="slides_3"></label>
			    <label for="slides_4"></label>
			    <label for="slides_5"></label>
			    <label for="slides_6"></label>
			    <label for="slides_7"></label>
			    <label for="slides_8"></label>
			    <label for="slides_1" class="goto-first"></label>
			    <label for="slides_8" class="goto-last"></label>
			  </div>
			  <div class="navigation"> 
			    <div>
			      <label for="slides_1"></label>
			      <label for="slides_2"></label>
			      <label for="slides_3"></label>
			      <label for="slides_4"></label>
			      <label for="slides_5"></label>
			      <label for="slides_6"></label>
			      <label for="slides_7"></label>
			      <label for="slides_8"></label>
			    </div>
			  </div>
		</div>
	</section>

	<section class="testimonials desktop-tc mobile-hide">
			<h2 class="is-center">What Our Clients Say</h2>

			<div class="gallery items-3">

  				<div id="item-1" class="control-operator"></div>
  				<div id="item-2" class="control-operator"></div>

  				<div class="secondary-controls">
					<div class="superfluous">
						<nav>
							<a class="arrow-previous" href="#item-1"></a>
							<a class="arrow-next" href="#item-2"></a>
						</nav>
					</div>
				</div>
		
				<figure class="item">
					<img src="/wp-content/themes/Rehab-And-Treatment/mobile-images/testimonials/RichardS.jpg"><blockquote class="testimonial-quote">My favorite part of the program was the individualized treatment and the personal relationships that I had with the therapists, counselors and staff. At any given moment if I needed to sit down with anyone, they were there to talk. It didn’t matter what time of day it was.<br><cite>Richard S.</cite></blockquote>
					<img src="/wp-content/themes/Rehab-And-Treatment/mobile-images/testimonials/VanessaW.jpg"><blockquote class="testimonial-quote">The person I was when I entered treatment was very dark. I didn’t think that they would get me, that they would understand what it was that I was going through. So, from the beginning I had a rough time opening up to them. By the end of my stay, they were friends.<br><cite>Vanessa W.</cite></blockquote>
					<div style="clear:both; padding:2em;"></div>
					<img src="/wp-content/themes/Rehab-And-Treatment/mobile-images/testimonials/BrentW.jpg"><blockquote class="testimonial-quote">My life was centered around my addiction. If I hadn't gotten the help that I needed at Foundations, I would have been dead or held up in my parents' home living a miserable existence.<br><cite>Brent W.</cite></blockquote>
					<img src="/wp-content/themes/Rehab-And-Treatment/mobile-images/testimonials/MarkS.jpg"><blockquote class="testimonial-quote">My parents feared for my life. Things were getting out of control. If someone would have told me that it would have been as easy as picking up the phone for Foundations, I would definitely pick up the phone and call. It's the best thing I could have ever done.<br><cite>Mark S.</cite></blockquote>
				</figure>

				<figure class="item">
					<img src="/wp-content/themes/Rehab-And-Treatment/mobile-images/testimonials/CristinaL.jpg"><blockquote class="testimonial-quote">The treatment plan was customized for my needs. As my needs changed, the program changed for what I needed it to be. This program is about giving yourself the chance and the opportunity to become all those things that you don’t think you could ever become. Just give yourself the chance.<br><cite>Christina L.</cite></blockquote>
					<img src="/wp-content/themes/Rehab-And-Treatment/mobile-images/testimonials/JanetM.jpg"><blockquote class="testimonial-quote">This is a place that’s so nurturing, that’s going to help you get through the problems you have to get through. They’re not just telling you how to get better. They’re showing you and giving you life lessons. They gave me my spirit back.<br><cite>Janet M.</cite></blockquote>
					<div style="clear:both; padding:2em;"></div>
					<img src="/wp-content/themes/Rehab-And-Treatment/mobile-images/testimonials/JenH.jpg"><blockquote class="testimonial-quote">Foundations isn’t just a detox center. They take special care to try to understand what those things are that make a person use in the first place and address them, giving you very personalized care. At the most desperate point in your life, Foundations will be there. There is hope.<br><cite>Jen H.</cite></blockquote>
					<img src="/wp-content/themes/Rehab-And-Treatment/mobile-images/testimonials/MarthaF.jpg"><blockquote class="testimonial-quote">Before my son went to Foundations, there were people who said to me that he was hopeless. But that just wasn’t true. Nothing else worked until we found Foundations.<br><cite>Martha F.</cite></blockquote>
				</figure>

				<div class="controls">
					<a href="#item-1" class="control-button">•</a>
					<a href="#item-2" class="control-button">•</a>
				</div>

			</div>
			
		
	</section>


	<section class="cost-of-treatment greybg" id="cost-of-treatment">
		<div class="inner">
			<h2 class="is-center">Cost of Treatment</h2>
			<p class="is-center">Our top priority is to get you or your loved one healthy again, so we make sure the worries of cost and payment don’t get in the way. Instead of a bottom-line price for treatment, we calculate a payment plan realistic for you and your family by looking at:</p>
			<div class="cot-list">
				<ul>
					<li><i class="fa fa-check" aria-hidden="true"></i> Your financial situation</li>
					<li><i class="fa fa-check" aria-hidden="true"></i> Any insurance coverage</li>
					<li><i class="fa fa-check" aria-hidden="true"></i> Your specific medical conditions</li>
					<li><i class="fa fa-check" aria-hidden="true"></i> Many other factors</li>
					<li><i class="fa fa-check" aria-hidden="true"></i> Your unique treatment needs</li>
				</ul>
				<p class="ta-l"><strong><i>You can rest assured that our seasoned admissions team will exhaust every option to get you or your loved one into treatment hassle-free and affordably.</i></strong></p>
			</div>
		</div>
	</section>

	<section class="insurance">
		<h2 class="is-center">We work with all the major health insurance providers…</h2>
		<hr>
		<div class="pure-g i-icons">
			<div class="pure-u-1-2 pure-u-md-1-5">
				<img src="/wp-content/themes/Rehab-And-Treatment/mobile-images/insurance-icons/aetna_large1.png">
			</div>
			<div class="pure-u-1-2 pure-u-md-1-5">
				<img src="/wp-content/themes/Rehab-And-Treatment/mobile-images/insurance-icons/bcbshield_large1.png">
			</div>
			<div class="pure-u-1-2 pure-u-md-1-5">
				<img src="/wp-content/themes/Rehab-And-Treatment/mobile-images/insurance-icons/communityhealthalliance1.png">
			</div>
			<div class="pure-u-1-2 pure-u-md-1-5">
				<img src="/wp-content/themes/Rehab-And-Treatment/mobile-images/insurance-icons/humana_large1.png">
			</div>
			<div class="pure-u-1-2 pure-u-md-1-5">
				<img src="/wp-content/themes/Rehab-And-Treatment/mobile-images/insurance-icons/magellanhealth_large1.png">
			</div>
			<div class="pure-u-1-2 pure-u-md-1-5">
				<img src="/wp-content/themes/Rehab-And-Treatment/mobile-images/insurance-icons/multiplan_Large1.png">
			</div>
			<div class="pure-u-1-2 pure-u-md-1-5">
				<img src="/wp-content/themes/Rehab-And-Treatment/mobile-images/insurance-icons/POMCO.png">
			</div>
			<div class="pure-u-1-2 pure-u-md-1-5">
				<img src="/wp-content/themes/Rehab-And-Treatment/mobile-images/insurance-icons/UMR.png">
			</div>
			<div class="pure-u-1-2 pure-u-md-1-5">
				<img src="/wp-content/themes/Rehab-And-Treatment/mobile-images/insurance-icons/unitedhealthcare_large.png">
			</div>
			<div class="pure-u-1-2 pure-u-md-1-5">
				<img src="/wp-content/themes/Rehab-And-Treatment/mobile-images/insurance-icons/valueoptions_large1-150x34.png">
			</div>
		</div>
		<hr>
		<h2 class="is-center">…and many more!</h2>
		<div class="l-box is-center"><p><i><strong>BONUS: </strong>Our dedicated benefits verification team handles all the details with your health insurance provider so you don’t have to! Currently, we don’t accept clients with Medicare or Medicaid into our inpatient facilities, but we do have the ability to refer out to other locations that can accept that kind of coverage.</i></p>
		</div>
	</section>

	<section class="cta-phone" id="expect-call">
		<h3 class="is-center">The path to recovery begins with just a simple phone call.</h3>
		<h2 class="is-center green-cta"><?php echo do_shortcode('[frn_phone action="Phone Clicks in Outpatient Landing Page CTA Above Footer"]'); ?></h2>
		<p class="is-center">Give us a call.</p>
	</section>

	<!-- SCROLL TO TOP BUTTON -->
	<a href="#scrolltotop" class="scrollToTop"><i class="fa fa-caret-up"></i></a>

	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>
	<?php wp_footer(); ?>
	
	<?php /* 
	<footer>
		<img class="pure-img-responsive" src="/wp-content/themes/Rehab-And-Treatment/mobile-images/FRN-logo-dark.png">
		<p>Copyright © 1995-2016 Foundations Recovery Network. All Rights Reserved. | Confidential and Private Call: <strong><?php echo do_shortcode('[frn_phone action="Phone Clicks in Landing Page Footer"]'); ?></strong> | Privacy Policy</p>
	</footer>
	<?php */ ?>
	
</div><!-- end content -->
</div><!-- end content-wrapper -->

	<link rel='stylesheet' id='outpatient-ppc'  href='/wp-content/themes/Rehab-And-Treatment/css/layouts/outpatient-ppc.css' type='text/css' media='all' />
	<?php if(function_exists('frn_enqueue_script')) : //verifies that the FRN Plugin is activated 
	?><link rel='stylesheet' id='frn_plugin'  href='<?=plugins_url('frn_plugins/frn_plugin.css');?>?v=1.12' type='text/css' media='all' />
	<?php endif; ?>
<?php /*
	// Dax removed this 10/19/17 since the FRN plugin now provides this
?>
	<!-- #####
	IfByPHone Phone Number Code
	##### -->
	<script type="text/javascript">
		var _stk = "29b7c6a9512f3b1450d85acb9aa55fb8818ba6aa";

		(function(){
			var a=document, b=a.createElement("script"); b.type="text/javascript";
			b.async=!0; b.src=('https:'==document.location.protocol ? 'https://' :
			'http://') + 'd31y97ze264gaa.cloudfront.net/assets/st/js/st.js';
			a=a.getElementsByTagName("script")[0]; a.parentNode.insertBefore(b,a);
		})();
	</script>
	<!-- #####
	End IfByPhone
	##### -->

		<!-- #####
	
	##### -->
<?php */ 



	global $frn_mobile;
    if($frn_mobile=="Smartphone") : 
    //Used by Dax on 10/18/17 to remove this code since it's unnecessary for desktop/ipad users ?>
    
	<!-- BEGIN SCROLL TO TOP BUTTON -->
	<script type="text/javascript">
	$(document).ready(function(){
		//Check to see if the window is top if not then display button
		$(window).scroll(function(){
			if ($(this).scrollTop() > 100) {
				$('.scrollToTop').fadeIn();
			} else {
				$('.scrollToTop').fadeOut();
			}
		});
		//Click event to scroll to top
		$('.scrollToTop').click(function(){
			$('html, body').animate({scrollTop : 0},800);
			return false;
		});			
	});
	</script>
	<!--  END SCROLL TO TOP BUTTON -->
<?php endif; ?>

	<!--  BEGIN FLOATING BAR -->
	<script type="text/javascript">
	$(function() {
	//caches a jQuery object containing the header element
	var header = $("#mobile-nav");
	$(window).scroll(function() {
	    var scroll = $(window).scrollTop();

	    if (scroll >= 80) {
	        header.removeClass('hidden2').addClass("visible");
	    } else {
	        header.removeClass("visible").addClass('hidden2');
	    }
	});
	});
	</script>
	<!--  END FLOATING BAR -->

	<!--  BEGIN SMOOTH SCROLL -->
	<script type="text/javascript">
	$(function() {
	$('a.smoothscroll[href*="#"]:not([href=""])').click(function() {
	if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
	  var target = $(this.hash);
	  target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
	  if (target.length) {
	    $('html, body').animate({
	      scrollTop: target.offset().top - 70
	    }, 1100);
	    return false;
	  }
	}
	});
	});
	</script>
	<!--  END SMOOTH SCROLL -->
	<?php /* // Dax removed this 10/19/17 since the FRN plugin now provides this ?>
	<script src="https://cdn.optimizely.com/js/1033590360.js"></script>
	<?php */ ?>

	<link rel="stylesheet" href="http://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css">
	<link href='https://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
	<link href='https://fonts.googleapis.com/css?family=Source+Sans+Pro:400,400i,600i,700' rel='stylesheet' type='text/css'>

</body>
</html>
