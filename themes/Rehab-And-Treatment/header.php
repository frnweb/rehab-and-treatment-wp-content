<!doctype html>

  <html class="no-js"  <?php language_attributes(); ?>>

	<head>
		<meta charset="utf-8">
		
		<!-- Force IE to use the latest rendering engine available -->
		<meta http-equiv="X-UA-Compatible" content="IE=edge">

		<!-- Mobile Meta -->
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta class="foundation-mq">
		
		<!-- If Site Icon isn't set in customizer -->
		<?php if ( ! function_exists( 'has_site_icon' ) || ! has_site_icon() ) { ?>
			<!-- Icons & Favicons -->
			<link rel="icon" href="<?php echo get_template_directory_uri(); ?>/favicon.png">
			<link href="<?php echo get_template_directory_uri(); ?>/assets/images/apple-icon-touch.png" rel="apple-touch-icon" />
			<!--[if IE]>
				<link rel="shortcut icon" href="<?php echo get_template_directory_uri(); ?>/favicon.ico">
			<![endif]-->
			<meta name="msapplication-TileColor" content="#f01d4f">
			<meta name="msapplication-TileImage" content="<?php echo get_template_directory_uri(); ?>/assets/images/win8-tile-icon.png">
	    	<meta name="theme-color" content="#121212">
	    <?php } ?>

		<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">
		<link href='https://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
	    <link href='https://fonts.googleapis.com/css?family=Source+Sans+Pro:400,400i,600i,700' rel='stylesheet' type='text/css'>
	    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>
	    <link rel="stylesheet" href="http://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css">

		<?php wp_head(); ?>

		<?php if(is_user_logged_in()) : ?>
		<style>
			html {
			    margin-top: 0 !important;
			}
		</style>
		<?php endif; ?>
<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-KZ4NPG5');</script>
<!-- End Google Tag Manager -->
	</head>
		
	<body <?php body_class(); ?>>

	<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-KZ4NPG5"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->

<div class="off-canvas-wrapper">
<div class="off-canvas-wrapper-inner" data-off-canvas-wrapper>
	
<?php get_template_part( 'parts/content', 'offcanvas' ); ?>


<div class="off-canvas-content" data-off-canvas-content>
	
	<header class="header" role="banner">
			
		<?php 
			/* 
			The navs will be applied to the topbar, above all content 
			To see additional nav styles, visit the /parts directory 
			*/ 
		?>
		<?php get_template_part( 'parts/nav', 'offcanvas-topbar' ); ?>


		<!-- FLOATING FOOTER BAR -->
		<div id="mobile-nav">
	    	<div class="row">
				<div class="small-2 medium-4 columns"><?php 
					echo do_shortcode('[lhn_inpage button="email" text="empty" desktop="Email" class="fa fa-envelope" category="Contact Options Bar" action="Email Clicks" ]');
					/* 
					?><a onclick="window.open('https://www.livehelpnow.net/lhn/TicketsVisitor.aspx?lhnid=14160','Ticket','left=' + (screen.width - 550-32) / 2 + ',top=50,scrollbars=yes,menubar=no,height=550,width=450,resizable=yes,toolbar=no,location=no,status=no');return false;"><i class="fa fa-envelope" aria-hidden="true"></i><span class="mobile-hide">Email</span></a>
					<?php */ 
				?></div>
				<div class="small-8 number medium-4 columns"><i class="fa fa-mobile mobile-hide" aria-hidden="true"></i><span class="phone-number-fixed"><?php 
					echo do_shortcode('[frn_phone action="Phone Clicks in Landing Page Menu Bar"]'); 
				?></span></div>
				<div class="small-2 medium-4 columns"><?php 
					echo do_shortcode('[lhn_inpage button="chat" text="empty" desktop="Live Chat" offline_mobile="empty" class="fa fa-comments-o" category="Contact Options Bar" action="Chat Clicks" ]'); 
					/* 
					?><a onclick="OpenLHNChat();return false; ga('send', 'event', 'Contact Options Flyout', 'Chat/Email');"><i class="fa fa-comments-o" aria-hidden="true"></i><span class="mobile-hide">Live Chat</span></a>
					<?php */ 
				?></div>
			</div>
		</div>
		<!-- END FLOATING FOOTER BAR -->

		 	
	</header> <!-- end .header -->


